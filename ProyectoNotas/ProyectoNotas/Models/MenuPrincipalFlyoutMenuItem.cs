﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoNotas.Models
{
    public class MenuPrincipalFlyoutMenuItem
    {
        public MenuPrincipalFlyoutMenuItem()
        {
            TargetType = typeof(MenuPrincipalFlyoutMenuItem);
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Foto { get; set; }
        public string EsVisible { get; set; }
        public Type TargetType { get; set; }
    }
}