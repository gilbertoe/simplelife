﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoNotas.Models
{
    public class ProyectosModel
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int IdUsuario { get; set; }
        public bool Habilitado { get; set; }
    }
}
