﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoNotas.Models
{

    public class OpcionesMenuModel
    {
        public int IdOpcionesMenu { get; set; }
        public string Nombre { get; set; }
        public string Foto { get; set; }
        public bool Visible { get; set; }
        public int Orden { get; set; }
    }
}
