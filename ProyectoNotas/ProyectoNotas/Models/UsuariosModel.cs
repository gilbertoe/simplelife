﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoNotas.Models
{
    public class UsuariosModel
    {
        public int ID { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string CodigoConfirmacion { get; set; }
        public string Email { get; set; }
        public string Contrasena { get; set; }
        public string Telefono { get; set; }
        public byte[] Foto { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int Estatus { get; set; }
        public DateTime FechaBaja { get; set; }
        public bool Validado { get; set; }

    }
}
