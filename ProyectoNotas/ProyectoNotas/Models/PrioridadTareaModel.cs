﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoNotas.Models
{
    public class PrioridadTareaModel
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public bool Habilitado { get; set; }
    }
}
