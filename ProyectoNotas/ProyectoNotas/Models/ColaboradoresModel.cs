﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoNotas.Models
{
    public class ColaboradoresModel
    {
        public int ID { get; set; }
        public int IdUsuario { get; set; }
        public int IdColaborador { get; set; }
        public int IdProyecto { get; set; }
        public bool InvitacionAceptada { get; set; }
        public bool ColaboracionVigente { get; set; }
        public string CodigoColaboracion { get; set; }
        public string StatusColaboracion { get; set; }
        public int IdStatusColaboracion { get; set; }
        public string NombreProyecto { get; set; }
        public UsuariosModel Usuario { get; set; } = new UsuariosModel();
    }
}
