﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoNotas.Models
{
    public class TareasModelSQLite
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public int IdUsuario { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public int IdEstatus { get; set; }
        public int IdPrioridad { get; set; }
        public int IdProyecto { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
    }
}
