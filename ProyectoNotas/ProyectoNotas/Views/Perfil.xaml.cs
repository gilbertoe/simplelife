﻿using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using Newtonsoft.Json;
using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Rg.Plugins.Popup.Services;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.Threading;
using System.IO;
using System.Threading.Tasks;
using Acr.UserDialogs;

namespace ProyectoNotas.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Perfil : ContentPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        private MediaFile FotoLocal = null;
        public Perfil(bool fromMenu)
        {
            InitializeComponent();
            Device.BeginInvokeOnMainThread(async () =>
            {
                using (UserDialogs.Instance.Loading(("Cargando...")))
                {
                    await Task.Delay(300);
                    if (fromMenu)
                    {
                        btnCerrar.IsEnabled = true;
                        btnCerrar.IsVisible = true;
                    }
                    else
                    {
                        btnCerrar.IsEnabled = false;
                        btnCerrar.IsVisible = false;
                    }
                    UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
                    txtApellidoPaterno.Text = UserLogged.ApellidoPaterno;
                    txtApellidoMaterno.Text = UserLogged.ApellidoMaterno;
                    txtNombres.Text = UserLogged.Nombres;
                    txtTelefono.Text = UserLogged.Telefono;
                    if (UserLogged.Foto != null)
                    {
                        imgPerfil.Source = ImageSource.FromStream(() => new MemoryStream(UserLogged.Foto));
                        imgPerfil.Aspect = Aspect.AspectFill;

                    }
                }
           });

        }
        #region Events
        private void btnGuardar_Clicked(object sender, EventArgs e)
        {
            Actualizar();
        }
        private void btnActualizarPass_Clicked(object sender, EventArgs e)
        {
            ActualizarPass objActualizarPass = new ActualizarPass();
            objActualizarPass.CloseWhenBackgroundIsClicked = true;
            PopupNavigation.Instance.PushAsync(objActualizarPass);
        }
        private void btnCerrar_Clicked(object sender, EventArgs e)
        {
            NavigateAsync();
        }
        private void btnFoto_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                string action = await DisplayActionSheet("De donde quiere seleccionar la foto?", "Cancelar", null, "Cámara", "Galería");
                if (action == "Cámara")
                {
                    TomarFoto();
                }
                if (action == "Galería")
                {
                    SeleccionarGaleria();
                }
            });
        }
        #endregion
        #region Metodos
        private void TomarFoto()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        FotoLocal = null;
                        await CrossMedia.Current.Initialize();

                        if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                        {
                            await DisplayAlert("Sin permisos", "No hay permisos para usar la cámara", "OK");
                            return;
                        }
                        MediaFile file;
                        file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                        {
                            Directory = "Xamarin",
                            SaveToAlbum = true,
                            CompressionQuality = 50,
                            PhotoSize = PhotoSize.Medium,
                            DefaultCamera = CameraDevice.Front
                        });

                        if (file == null) { return; }

                        //await DisplayAlert("Ubicación del Archivo", file.Path, "OK");

                        FotoLocal = file;
                        imgPerfil.Source = ImageSource.FromStream(() =>
                        {
                            var stream = file.GetStream();
                            return stream;
                        });
                        imgPerfil.Aspect = Aspect.AspectFill;
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: Perfil, Método: TomarFoto, Error: " + ex.Message);
            }
        }
        private void SeleccionarGaleria()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        FotoLocal = null;
                        if (!CrossMedia.Current.IsPickPhotoSupported)
                        {
                            await DisplayAlert("Sin permisos", "No hay permisos para usar la Galería", "OK");
                            return;
                        }
                        MediaFile file;
                        file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                        {
                            CompressionQuality = 50,
                            CustomPhotoSize = 50,
                            PhotoSize = PhotoSize.Medium
                        });

                        if (file == null) { return; }
                        Thread.Sleep(1000);
                        FotoLocal = file;
                        imgPerfil.Source = ImageSource.FromStream(() =>
                        {
                            var stream = file.GetStream();
                        //file.Dispose();
                        return stream;
                        });
                        imgPerfil.Aspect = Aspect.AspectFill;
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: Perfil, Método: SeleccionarGaleria, Error: " + ex.Message);
            }
        }
        private void NavigateAsync()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await Navigation.PopModalAsync();
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: Perfil, Método: NavigateAsync, Error: " + ex.Message);
            }
        }
        private void Actualizar()
        {

            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        string MensajeValidarCampos = ValidarCampos();
                        if (!string.IsNullOrEmpty(MensajeValidarCampos))
                        {
                            await DisplayAlert("Notificación", MensajeValidarCampos, "OK");
                            return;
                        }
                        if (txtTelefono.Text != "")
                        {
                            if (!generalHelper.ValidarTelefono(txtTelefono.Text))
                            {
                                await DisplayAlert("Notificación", "El telefono ingresado no es válido", "OK");
                                return;
                            }
                        }
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));

                        ActualizarUsuario(UserLogged);
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: Registro, Método: Registrar, Error: " + ex.Message);
            }

        }
        private void ActualizarUsuario(UsuariosModel objUsuarioModel)
        {
            try
            {
                objUsuarioModel.ApellidoMaterno = txtApellidoMaterno.Text;
                objUsuarioModel.ApellidoPaterno = txtApellidoPaterno.Text;
                objUsuarioModel.Nombres = txtNombres.Text;
                objUsuarioModel.Telefono = txtTelefono.Text;
                if (FotoLocal != null)
                {
                    objUsuarioModel.Foto =  FotoABytes();
                }
                else
                {
                    objUsuarioModel.Foto = null;
                }
                UsuariosAdapters objUsuariosAdapter = new UsuariosAdapters();
                if (!objUsuariosAdapter.ActualizarUsuario(objUsuarioModel))
                {
                    DisplayAlert("Notificación", "Sucedió un fallo al intentetar actualizar, intenta más tarde", "OK");
                    return;
                }
                generalHelper.GuardarSesionLocal(objUsuarioModel);
                DisplayAlert("Notificación", "Datos Actualizados", "OK");
                NavigateAsync();

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: Perfil, Método: ActualizarUsuario, Error: " + ex.Message);

            }
        }
        private byte[] FotoABytes()
        {
            byte[] imageArray = null;
            try
            {
                using (MemoryStream memory = new MemoryStream())
                {
                    Stream stream = FotoLocal.GetStream();
                    stream.CopyTo(memory);
                    imageArray = memory.ToArray();
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: Perfil, Método: FotoABytes, Error: " + ex.Message);
            }
            return imageArray;
        }
        private string ValidarCampos()
        {
            if (string.IsNullOrEmpty(txtNombres.Text))
            {
                return "Nombres vacíos";
            }
            else if (string.IsNullOrEmpty(txtApellidoPaterno.Text))
            {
                return "Apellido paterno vacío";
            }
            return "";
        }
        #endregion



    }
}