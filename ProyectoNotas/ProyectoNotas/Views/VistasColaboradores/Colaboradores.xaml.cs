﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views.VistasColaboradores
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Colaboradores : ContentPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        public Colaboradores()
        {
            InitializeComponent();
            //lstColaboradores.RefreshCommand = new Command(() => {
            //    lstColaboradores.IsRefreshing = true;
            LlenarListado(0);
            //    lstColaboradores.IsRefreshing = false;
            //});
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LlenarListado(0);
        }
        private void smgFiltrarColaboradores_OnSegmentSelected(object sender, Plugin.Segmented.Event.SegmentSelectEventArgs e)
        {
            this.LlenarListado(smgFiltrarColaboradores.SelectedSegment);

        }
        private void lstColaboradores_Refreshing(object sender, EventArgs e)
        {
            LlenarListado(0);
            lstColaboradores.IsRefreshing = false;
        }
        private void lstColaboradores_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

        }
        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            RemoverColaborador(sender);
        }
        private void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new InvitarColaboracion());

        }
        private void RemoverColaborador(object sender)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        MenuItem viewCellSelected = sender as MenuItem;
                        ColaboradoresModel ColaboradorRemover = viewCellSelected?.BindingContext as ColaboradoresModel;
                        ColaboradoresAdapter objColaboradorAdapter = new ColaboradoresAdapter();
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        if (objColaboradorAdapter.RemoverColaboracion(ColaboradorRemover.ID))
                        {
                            await DisplayAlert("Notificación", "Colaborador removido del proyecto", "OK");
                            LlenarListado(smgFiltrarColaboradores.SelectedSegment);
                        }
                        else
                        {
                            await DisplayAlert("Intenta más tarde ", "Ocurrió un fallo al intentar eliminar la tarea", "OK");
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: Colaboradores.xaml.cs, Method: RemoverColaborador, Error: " + ex.Message);
            }

        }
        private void LlenarListado(int EsInvitado)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        ColaboradoresAdapter objColaboradoresAdapter = new ColaboradoresAdapter();
                        UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        List<ColaboradoresModel> listColaboradores = new List<ColaboradoresModel>();
                        listColaboradores = objColaboradoresAdapter.ObtenerColaboradoresUsuario(UserLogged.ID, EsInvitado);
                        lstColaboradores.ItemsSource = listColaboradores;
                        if (listColaboradores.Count == 0)
                        {
                            lblNoTareas.IsVisible = true;
                        }
                        else
                        {
                            lblNoTareas.IsVisible = false;
                        }
                    }
                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: Colaboradores.xaml.cs, Method: LlenarListado, Error: " + ex.Message);

            }
        }

       
    }
}