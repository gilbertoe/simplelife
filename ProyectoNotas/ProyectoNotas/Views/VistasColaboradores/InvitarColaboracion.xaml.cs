﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views.VistasColaboradores
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InvitarColaboracion : ContentPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        public InvitarColaboracion()
        {
            InitializeComponent();
            LlenarPickers();
        }

        private void btnValidarCorreo_Clicked(object sender, EventArgs e)
        {
            ValidarCorreo();
        }

        private void btnEnviarInvitacion_Clicked(object sender, EventArgs e)
        {
            EnviarInvitacion();
        }

        private void EnviarInvitacion()
        {
            try
            {
                
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        if (string.IsNullOrEmpty(lblIdColaborador.Text))
                        {
                            await DisplayAlert("Notificación", "Por favor selecciona un colaborador válido", "OK");
                            return;
                        }
                        if (!generalHelper.ValidarEmail(txtCorreo.Text))
                        {
                            await DisplayAlert("Notificación", "El email ingresado no es válido", "OK");
                            return;
                        }

                        Random generator = new Random();
                        String RandomNumber = generator.Next(0, 1000000).ToString("D6");
                        UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));

                        ColaboradoresModel nuevaColaboracion = new ColaboradoresModel();
                        nuevaColaboracion.IdUsuario = UserLogged.ID;
                        nuevaColaboracion.IdColaborador = int.Parse(lblIdColaborador.Text);
                        nuevaColaboracion.IdProyecto = ((ProyectosModel)pckProyectos.SelectedItem).ID;
                        nuevaColaboracion.CodigoColaboracion = RandomNumber;
                        nuevaColaboracion.Usuario.Email = txtCorreo.Text;
                        nuevaColaboracion.Usuario.Nombres = txtNombres.Text;

                        ColaboradoresAdapter objColaborador = new ColaboradoresAdapter();
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        if (!objColaborador.GuardarColaboracion(nuevaColaboracion))
                        {
                            await DisplayAlert("Problemas al solicitar colaboración", "Por favor intenta más tarde", "OK");
                            return;
                        }

                        if (!mailinghelper.EnviarInvitacionColaborar(nuevaColaboracion, UserLogged.Email, ((ProyectosModel)pckProyectos.SelectedItem).Nombre))
                        {
                            await DisplayAlert("Problemas el envia email", "Tuvimos un problema al envia la invitación, Por favor intenta más tarde", "OK");
                            return;
                        }
                        await DisplayAlert("Notificación", "Enviamos la invitación a tu futuro colaborado, espera su respuesta :)", "OK");
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: InvitarColaboracion, Método: EnviarInvitacion, Error: " + ex.Message);

            }
        }
        private void ValidarCorreo()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));

                        if (UserLogged.Email == txtCorreo.Text)
                        {
                            await DisplayAlert("Notificación", "El email ingresado no es válido", "OK");
                            return;
                        }

                        if (!generalHelper.ValidarEmail(txtCorreo.Text))
                        {
                            await DisplayAlert("Notificación", "El email ingresado no es válido", "OK");
                            return;
                        }
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }

                        UsuariosModel objUsuariosModel = new UsuariosModel();
                        ColaboradoresAdapter objColaboradoresAdapter = new ColaboradoresAdapter();
                        objUsuariosModel = objColaboradoresAdapter.ExtraerColaborador(txtCorreo.Text);
                        if (objUsuariosModel.ID <= 0)
                        {
                            await DisplayAlert("Notificación", "Perfil no encontrado, intenta con otro Email", "OK");
                            return;
                        }
                        else
                        {
                            lblTituloNombre.IsVisible = true;
                            imgNombre.IsVisible = true;
                            txtNombres.IsVisible = true;

                            lblTituloApellido.IsVisible = true;
                            txtApellidoPaterno.IsVisible = true;
                            imgApellido.IsVisible = true;

                            txtNombres.Text = objUsuariosModel.Nombres;
                            txtApellidoPaterno.Text = objUsuariosModel.ApellidoPaterno;
                            lblIdColaborador.Text = objUsuariosModel.ID.ToString();
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: InvitarColaboracion, Método: ValidarCorreo, Error: " + ex.Message);
            }
        }

        private void LlenarPickers()
        {
            try
            {
                Device.BeginInvokeOnMainThread( async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        ProyectosAdapters objProyectosAdapter = new ProyectosAdapters();
                        PrioridadTareaAdapter objPrioridadTareaAdapter = new PrioridadTareaAdapter();
                        UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
                        List<ProyectosModel> lstProyectos = objProyectosAdapter.ExtraerProyectosUsuario(UserLogged.ID);
                        pckProyectos.ItemsSource = lstProyectos;
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: InvitarColaboracion, Método: LlenarPickers, Error: " + ex.Message);

            }
        }

       

    }
}