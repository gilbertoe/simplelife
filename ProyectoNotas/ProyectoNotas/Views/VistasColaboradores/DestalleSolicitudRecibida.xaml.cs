﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views.VistasColaboradores
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DestalleSolicitudRecibida : PopupPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        //private int IdColaboracionLocal;
        ColaboradoresModel ColaboracionLocal = new ColaboradoresModel();

        public DestalleSolicitudRecibida(ColaboradoresModel Colaboracion)
        {
            InitializeComponent();
            ColaboracionLocal = Colaboracion;
        }

        private void btnCerrar_Clicked(object sender, EventArgs e)
        {
            PopupNavigation.Instance.PopAsync();
        }

        private void btnAceptar_Clicked(object sender, EventArgs e)
        {
            ColaboracionLocal.IdStatusColaboracion = 1;
            ColaboracionLocal.ColaboracionVigente = true;
            ColaboracionLocal.InvitacionAceptada = true;
            ActualizarColaboracion(ColaboracionLocal);
        }

        private void btnRechazar_Clicked(object sender, EventArgs e)
        {
            ColaboracionLocal.IdStatusColaboracion = 2;
            ColaboracionLocal.ColaboracionVigente = false;
            ColaboracionLocal.InvitacionAceptada = false;
            ActualizarColaboracion(ColaboracionLocal);

           
        }

        private void ActualizarColaboracion(ColaboradoresModel colaboracionLocal)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        ColaboradoresAdapter objColaboradoresAdapter = new ColaboradoresAdapter();
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        if (objColaboradoresAdapter.ActualizarColaboracion(colaboracionLocal))
                        {
                            UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
                            if (colaboracionLocal.IdStatusColaboracion == 1)
                            {
                                await DisplayAlert("Notificación", "Aceptaste la solicitud con éxito", "OK");
                                if (!mailinghelper.EnviarRespuestaColaborar(colaboracionLocal, UserLogged.Email, "Aceptado"))
                                {

                                }
                            }
                            if (colaboracionLocal.IdStatusColaboracion == 2)
                            {
                                await DisplayAlert("Notificación", "Rechazaste la solicitud con éxito", "OK");
                                if (!mailinghelper.EnviarRespuestaColaborar(colaboracionLocal, UserLogged.Email, "Rechazado"))
                                {

                                }
                            }
                            await PopupNavigation.Instance.PopAsync();

                        }
                        else
                        {
                            await DisplayAlert("Por favor intenta más tarde", "Tuvimos un problema al procesar tu solicitud", "OK");
                        }
                    }
                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: DestalleSolicitudRecibida.xaml.cs, Method: ActualizarColaboracion, Error: " + ex.Message);

            }
        }
    }
}