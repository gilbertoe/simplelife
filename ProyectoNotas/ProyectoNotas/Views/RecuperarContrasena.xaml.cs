﻿using Acr.UserDialogs;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecuperarContrasena : PopupPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();

        public RecuperarContrasena()
        {
            InitializeComponent();
        }
        #region Events
        private void btnCerrar_Clicked(object sender, EventArgs e)
        {
            PopupNavigation.Instance.PopAsync();
        }
        private void btnRecuperarContrasena_Clicked(object sender, EventArgs e)
        {
             EnviarRecuperarContrasena();
        }
        #endregion

        #region Metodos
        private void EnviarRecuperarContrasena()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        if (string.IsNullOrEmpty(txtEmail.Text)) 
                        { 
                            lblMensaje.Text = "Favor de ingresar tu email"; 
                            return; 
                        }
                        if (!generalHelper.ValidarEmail(txtEmail.Text)) 
                        { 
                            lblMensaje.Text = "Formato de email no válido"; 
                            return; 
                        }
                        UsuariosAdapters objUsuarioAdapter = new UsuariosAdapters();
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        string Contrasena = objUsuarioAdapter.ExtraerContrasena(txtEmail.Text);
                        if (string.IsNullOrEmpty(Contrasena)) 
                        { 
                            lblMensaje.Text = "El email ingresado no coincide con ninguna cuenta"; 
                            return; 
                        }
                        UsuariosModel objUsuarioModel = new UsuariosModel();
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        objUsuarioModel = objUsuarioAdapter.IniciarSesion(txtEmail.Text, Contrasena);
                        if (objUsuarioModel.ID > 0)
                        {
                            mailinghelper.EnviarContrasena(objUsuarioModel);
                            await PopupNavigation.Instance.PopAsync();
                        }
                        else
                        {
                            await DisplayAlert("Problemas al recuperar datos", "Intenta más tarde", "OK");

                        }
                    }
                       
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: RecuperarContrasena, Método: EnviarRecuperarContrasena, Error: " + ex.Message);
            }
        }
      
        #endregion

    }
}