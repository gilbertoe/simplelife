﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActualizarPass : PopupPage
    {

        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        public ActualizarPass()
        {
            InitializeComponent();
        }
        #region Events
        private void btnCerrar_Clicked(object sender, EventArgs e)
        {
            PopupNavigation.Instance.PopAsync();
        }
        private void btnGuardar_Clicked(object sender, EventArgs e)
        {
            ActualizarPassword();
        }
        private void btnVerPawssword_Clicked(object sender, EventArgs e)
        {
            if (txtPassword.IsPassword) { txtPassword.IsPassword = false; return; }
            txtPassword.IsPassword = true;
        }
        #endregion
        #region Metodos
        private void ActualizarPassword()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        string MensajeValidarCampos = ValidarCampos();
                        if (!string.IsNullOrEmpty(MensajeValidarCampos))
                        {
                            await DisplayAlert("Notificación", MensajeValidarCampos, "OK");
                            return;
                        }

                        if (!generalHelper.ValidarContrasena(txtPassword.Text))
                        {
                            await DisplayAlert("Notificación", "La contraseña debe tener al menos 1 número, 1 letra, entre 8 y 12 caracteres", "OK");
                            return;
                        }
                        if (!txtPassword.Text.Equals(txtRepitePassword.Text))
                        {
                            await DisplayAlert("Notificación", "Las contraseñas no son iguales", "OK");
                            return;
                        }
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }

                        Actualizar(txtPassword.Text);
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: ActualizarPass, Método: ActualizarPassword, Error: " + ex.Message);
            }

        }
        private void Actualizar(string NuevoPass)
        {
            try
            {
                UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
                UsuariosAdapters objUsuariosAdapter = new UsuariosAdapters();
                if (!objUsuariosAdapter.ActualizarPassword(UserLogged.ID, NuevoPass))
                {
                    DisplayAlert("Notificación", "Sucedió un fallo al intentetar actualizar, intenta más tarde", "OK");
                    return;
                }
                UserLogged.Contrasena = NuevoPass;
                generalHelper.GuardarSesionLocal(UserLogged);
                DisplayAlert("Notificación", "Datos Actualizados", "OK");
                PopupNavigation.Instance.PopAsync();

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: ActualizarPass, Método: Actualizar, Error: " + ex.Message);

            }
        }
        private string ValidarCampos()
        {
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                return "Contraseña vacío";
            }
            else if (string.IsNullOrEmpty(txtRepitePassword.Text))
            {
                return "Por favor repite el password";
            }
            return "";
        }
        #endregion
    }
}