﻿using ProyectoNotas.Helpers;
using ProyectoNotas.Adapters;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Acr.UserDialogs;

namespace ProyectoNotas.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnviarCodigo : PopupPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();

        public EnviarCodigo(string Email)
        {
            InitializeComponent();
            if (!string.IsNullOrEmpty(Email))
            {
                txtEmail.Text = Email;
                lblTextoMail.IsVisible = false;
                txtEmail.IsVisible = false;
            }
        }
        #region Events
        private void btnCerrar_Clicked(object sender, EventArgs e)
        {
            PopupNavigation.Instance.PopAsync();
        }
        private void btnValidarCuenta_Clicked(object sender, EventArgs e)
        {
            ValidarCuenta();
        }
        #endregion
        #region Metodos
        private void ValidarCuenta()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        if (!string.IsNullOrEmpty(ValidarCampos())) 
                        { 
                            await DisplayAlert("Notificación", ValidarCampos(), "OK");
                            return; 
                        }
                        if (!generalHelper.ValidarEmail(txtEmail.Text)) 
                        { 
                            await DisplayAlert("Notificación", "El email ingresado no es válido", "OK");
                            return; 
                        }
                        if (!generalHelper.ValidarConexionInternet()) 
                        { 
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK"); 
                            return; 
                        }
                        if (!ValidarUsuario()) 
                        { 
                            await DisplayAlert("Cuenta no validada", "Verfica que tu Email o Código sean correctos", "OK"); 
                            return; 
                        }
                        await DisplayAlert("Notificación", "Tu cuenta ha sido validada con éxito", "OK");
                        await Navigation.PushModalAsync(new Login());
                        await PopupNavigation.Instance.PopAsync();
                    }
                });
            }
            catch (Exception ex)
            {
                DisplayAlert("Error desconocido", "Por favor intenta de nuevo", "OK");
                mailinghelper.EnviarMensajeError("Clase: EnviarCodigo, Método: ValidarCuenta, Error: " + ex.Message);
            }
           
        }
        private bool ValidarUsuario()
        {
            try
            {
                UsuariosAdapters objUsuariosAdapter = new UsuariosAdapters();
                return objUsuariosAdapter.ValidarCuenta(txtEmail.Text, txtCodigo.Text);
            }
            catch
            {
                return false;
            }
        }
        private string ValidarCampos()
        {
            if (string.IsNullOrEmpty(txtEmail.Text)) return "Email vacío";
            if (string.IsNullOrEmpty(txtCodigo.Text)) return "Código vacío";
            return "";
        }
        #endregion
    }
}