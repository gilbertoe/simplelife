﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Rg.Plugins.Popup.Services;
using Xamarin.Essentials;
using ProyectoNotas.Helpers;
using ProyectoNotas.Adapters;
using ProyectoNotas.Models;
using ProyectoNotas.Views;
using ProyectoNotas.Views.VistasRegistro;
using Acr.UserDialogs;

namespace AppViajes.Views.VistasRegistro
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Registro : ContentPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();

        public Registro()
        {
            InitializeComponent();
        }
        #region Events
        private void btnVerPawssword_Clicked(object sender, EventArgs e)
        {
            if (txtPassword.IsPassword) { txtPassword.IsPassword = false; return; }
            txtPassword.IsPassword = true;
        }
        private void btnSiguiente_Clicked(object sender, EventArgs e)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        string MensajeValidarCampos = ValidarCampos();
                        if (!string.IsNullOrEmpty(MensajeValidarCampos))
                        {
                            await DisplayAlert("Notificación", MensajeValidarCampos, "OK");
                            return;
                        }
                        if (!generalHelper.ValidarEmail(txtEmail.Text))
                        {
                            await DisplayAlert("Notificación", "El email ingresado no es válido", "OK");
                            return;
                        }
                        if (!generalHelper.ValidarContrasena(txtPassword.Text))
                        {
                            await DisplayAlert("Notificación", "La contraseña debe tener al menos 1 número, 1 letra, entre 8 y 12 caracteres", "OK");
                            return;
                        }
                        if (!txtPassword.Text.Equals(txtRepitePassword.Text))
                        {
                            await DisplayAlert("Notificación", "Las contraseñas no son iguales", "OK");
                            return;
                        }
                        UsuariosAdapters objUsuariosAdapter = new UsuariosAdapters();
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        if (objUsuariosAdapter.ExisteUsuario(txtEmail.Text))
                        {
                            await DisplayAlert("Notificación", "Ya hay un usuario registrado con este Email", "OK");
                            return;
                        }
                        UsuariosModel objUsuarioModel = new UsuariosModel();
                        objUsuarioModel.Email = txtEmail.Text;
                        objUsuarioModel.Telefono = txtTelefono.Text;
                        objUsuarioModel.Contrasena = txtPassword.Text;
                        await Navigation.PushAsync(new RegistroPaso2(objUsuarioModel));
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: Registro, Método: Registrar, Error: " + ex.Message);
            }
        }
        #endregion
        #region Metodos

        private string ValidarCampos()
        {
            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                return "Email Vacío";
            }
            else if(string.IsNullOrEmpty(txtPassword.Text))
            {
                return "Contraseña vacía";
            }
            else if (string.IsNullOrEmpty(txtRepitePassword.Text))
            {
                return "Por favor repite el password";
            }
            return "";
        }
       
        #endregion

  
    }
}