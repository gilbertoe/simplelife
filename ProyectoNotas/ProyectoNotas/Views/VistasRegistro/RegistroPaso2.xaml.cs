﻿using Acr.UserDialogs;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views.VistasRegistro
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistroPaso2 : ContentPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        private UsuariosModel objUsuarioRegistrarLocal = new UsuariosModel();
        public RegistroPaso2(UsuariosModel objUsuarioRegistrar)
        {
            objUsuarioRegistrarLocal = objUsuarioRegistrar;
            InitializeComponent();
        }


        private void btnSiguiente_Clicked(object sender, EventArgs e)
        {
            this.Registrar(objUsuarioRegistrarLocal);
        }

        private void Registrar(UsuariosModel objUsuarioModel)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        string MensajeValidarCampos = ValidarCampos();
                        if (!string.IsNullOrEmpty(MensajeValidarCampos))
                        {
                            await DisplayAlert("Notificación", MensajeValidarCampos, "OK");
                            return;
                        }
                        if (!generalHelper.ValidarEmail(objUsuarioModel.Email))
                        {
                            await DisplayAlert("Notificación", "El email ingresado no es válido", "OK");
                            return;
                        }
                        if (!generalHelper.ValidarContrasena(objUsuarioModel.Contrasena))
                        {
                            await DisplayAlert("Notificación", "La contraseña debe tener al menos 1 número, 1 letra, entre 8 y 12 caracteres", "OK");
                            return;
                        }
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        UsuariosAdapters objUsuariosAdapter = new UsuariosAdapters();
                        if (objUsuariosAdapter.ExisteUsuario(objUsuarioModel.Email))
                        {
                            await DisplayAlert("Notificación", "Ya hay un usuario registrado con este Email", "OK");
                            return;
                        }
                        objUsuarioModel.Nombres = txtNombres.Text;
                        objUsuarioModel.ApellidoPaterno = txtApellidoPaterno.Text;
                        objUsuarioModel.ApellidoMaterno = txtApellidoMaterno.Text;

                        await Navigation.PushAsync(new RegistroPaso3(objUsuarioModel));
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: RegistroPaso2, Método: Registrar, Error: " + ex.Message);
            }

        }

        private string ValidarCampos()
        {
            if (string.IsNullOrEmpty(txtNombres.Text))
            {
                return "Nombre Vacío";
            }
            
            return "";
        }
       

       
    }
}