﻿using Acr.UserDialogs;
using Plugin.Media;
using Plugin.Media.Abstractions;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views.VistasRegistro
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistroPaso3 : ContentPage
    {

        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        private UsuariosModel objUsuarioRegistrarLocal = new UsuariosModel();
        private MediaFile FotoLocal = null;

        public RegistroPaso3(UsuariosModel objUsuarioRegistrar)
        {
            objUsuarioRegistrarLocal = objUsuarioRegistrar;
            InitializeComponent();
        }

        private void btnRegistro_Clicked(object sender, EventArgs e)
        {
            this.Registrar(objUsuarioRegistrarLocal);

        }
        private void btnFoto_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                string action = await DisplayActionSheet("De donde quiere seleccionar la foto?", "Cancelar", null, "Cámara", "Galería");
                if (action == "Cámara")
                {
                    TomarFoto();
                }
                if (action == "Galería")
                {
                    SeleccionarGaleria();
                }
            });
        }
        private void TomarFoto()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        FotoLocal = null;
                        await CrossMedia.Current.Initialize();

                        if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                        {
                            await DisplayAlert("Sin permisos", "No hay permisos para usar la cámara", "OK");
                            return;
                        }
                        MediaFile file;
                        file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                        {
                            Directory = "Xamarin",
                            SaveToAlbum = true,
                            CompressionQuality = 50,
                            PhotoSize = PhotoSize.Medium,
                            DefaultCamera = CameraDevice.Front
                        });

                        if (file == null) { return; }

                        await DisplayAlert("Ubicación del Archivo", file.Path, "OK");

                        FotoLocal = file;
                        imgPerfil.Source = ImageSource.FromStream(() =>
                        {
                            var stream = file.GetStream();
                            return stream;
                        });
                        imgPerfil.Aspect = Aspect.AspectFill;
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: Perfil, Método: TomarFoto, Error: " + ex.Message);
            }
        }
        private void SeleccionarGaleria()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        FotoLocal = null;
                        if (!CrossMedia.Current.IsPickPhotoSupported)
                        {
                            await DisplayAlert("Sin permisos", "No hay permisos para usar la Galería", "OK");
                            return;
                        }
                        MediaFile file;
                        file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                        {
                            CompressionQuality = 50,
                            CustomPhotoSize = 50,
                            PhotoSize = PhotoSize.Medium
                        });

                        if (file == null) { return; }
                        Thread.Sleep(1000);
                        FotoLocal = file;
                        imgPerfil.Source = ImageSource.FromStream(() =>
                        {
                            var stream = file.GetStream();
                        //file.Dispose();
                        return stream;
                        });
                        imgPerfil.Aspect = Aspect.AspectFill;
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: Perfil, Método: SeleccionarGaleria, Error: " + ex.Message);
            }
        }

        private void Registrar(UsuariosModel objUsuarioModel)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        if (!generalHelper.ValidarEmail(objUsuarioModel.Email))
                        {
                            await DisplayAlert("Notificación", "El email ingresado no es válido", "OK");
                            return;
                        }
                        if (!generalHelper.ValidarContrasena(objUsuarioModel.Contrasena))
                        {
                            await DisplayAlert("Notificación", "La contraseña debe tener al menos 1 número, 1 letra, entre 8 y 12 caracteres", "OK");
                            return;
                        }
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        UsuariosAdapters objUsuariosAdapter = new UsuariosAdapters();
                        if (objUsuariosAdapter.ExisteUsuario(objUsuarioModel.Email))
                        {
                            await DisplayAlert("Notificación", "Ya hay un usuario registrado con este Email", "OK");
                            return;
                        }
                        if (FotoLocal != null)
                        {
                            objUsuarioModel.Foto = FotoABytes();
                        }
                        else
                        {
                            objUsuarioModel.Foto = null;
                        }
                        RegistrarUsuario(objUsuariosAdapter, objUsuarioModel);
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: RegistroPaso2, Método: Registrar, Error: " + ex.Message);
            }

        }
        private void RegistrarUsuario(UsuariosAdapters objUsuariosAdapter, UsuariosModel objUsuarioModel)
        {
            try
            {

                Random generator = new Random();
                String RandomNumber = generator.Next(0, 1000000).ToString("D6");
                objUsuarioModel.CodigoConfirmacion = RandomNumber;
                int IdUsuario = objUsuariosAdapter.RegistrarUsuario(objUsuarioModel);
                if (IdUsuario <= 0) { DisplayAlert("Notificación", "Sucedió un fallo al intentetar registrar, intenta más tarde", "OK"); return; }
                mailinghelper.EnviarCodigoConfirmacion(RandomNumber, objUsuarioModel);
                EnviarCodigo objEnviarCodigo = new EnviarCodigo(objUsuarioModel.Email);
                objEnviarCodigo.CloseWhenBackgroundIsClicked = true;
                PopupNavigation.Instance.PushAsync(objEnviarCodigo);
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: RegistroPaso2, Método: RegistrarUsuario, Error: " + ex.Message);

            }

        }
        private byte[] FotoABytes()
        {
            byte[] imageArray = null;
            try
            {
                using (MemoryStream memory = new MemoryStream())
                {
                    Stream stream = FotoLocal.GetStream();
                    stream.CopyTo(memory);
                    imageArray = memory.ToArray();
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: Perfil, Método: FotoABytes, Error: " + ex.Message);
            }
            return imageArray;
        }
       

       
    }
}