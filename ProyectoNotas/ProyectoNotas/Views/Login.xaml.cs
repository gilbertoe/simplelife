﻿using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProyectoNotas.Views.Menu;
using ProyectoNotas.Models;
using ProyectoNotas.Helpers;
using ProyectoNotas.Adapters;
using AppViajes.Views;
using AppViajes.Views.VistasRegistro;
using Acr.UserDialogs;

namespace ProyectoNotas.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        public Login()
        {
            InitializeComponent();
        }

        #region Events
        private void btnLogin_Clicked(object sender, EventArgs e)
        {
            IniciarSesion();
        }
        private void btnRegistro_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Registro());
        }
        private void btnRecuperarContrasena_Clicked(object sender, EventArgs e)
        {
            RecuperarContrasena objRecuperarContrasena = new RecuperarContrasena();
            objRecuperarContrasena.CloseWhenBackgroundIsClicked = true;
            PopupNavigation.Instance.PushAsync(objRecuperarContrasena);
        }
        private void btnVerPawssword_Clicked(object sender, EventArgs e)
        {
            if (txtPassword.IsPassword) { txtPassword.IsPassword = false; return; }
            txtPassword.IsPassword = true;
        }
        private void btnValidarCuenta_Clicked(object sender, EventArgs e)
        {
            EnviarCodigo objEnviarCodigo = new EnviarCodigo("");
            objEnviarCodigo.CloseWhenBackgroundIsClicked = true;
            PopupNavigation.Instance.PushAsync(objEnviarCodigo);
        }
        #endregion

        #region Metodos
        private void IniciarSesion()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        string MensajeCampoVacio = ValidarCampos();
                        if (!string.IsNullOrEmpty(MensajeCampoVacio))
                        {
                            await DisplayAlert("Notificación", MensajeCampoVacio, "OK");
                            return;
                        }
                        if (!generalHelper.ValidarEmail(txtEmail.Text))
                        {
                            await DisplayAlert("Notificación", "El email ingresado no es válido", "OK");
                            return;
                        }
                        if (!generalHelper.ValidarContrasena(txtPassword.Text))
                        {
                            await DisplayAlert("Notificación", "La contraseña debe tener al menos 1 número, 1 letra, entre 8 y 12 caracteres", "OK");
                            return;
                        }
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }

                        UsuariosModel objUsuariosModel = new UsuariosModel();
                        UsuariosAdapters objUsuariosAdapter = new UsuariosAdapters();
                        if (!objUsuariosAdapter.ValidarCorreo(txtEmail.Text))
                        {
                            await DisplayAlert("Notificación", "No hay cuentas registradas con ese correo", "OK");
                            return;
                        }
                        if (!objUsuariosAdapter.ValidarContrasena(txtEmail.Text, txtPassword.Text))
                        {
                            await DisplayAlert("Notificación", "La contraseña no es correcta", "OK");
                            return;
                        }
                        objUsuariosModel = objUsuariosAdapter.IniciarSesion(txtEmail.Text, txtPassword.Text);
                        if (objUsuariosModel.ID <= 0)
                        {
                            await DisplayAlert("Notificación", "Lo sentitos tu cuenta no existe o no se encuentra verificada aún", "OK");
                            return;
                        }
                        generalHelper.GuardarSesionLocal(objUsuariosModel);
                        await Navigation.PushModalAsync(new MenuPrincipal());
                    }
                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: Login, Método: IniciarSesion, Error: " + ex.Message);
            }

        }
        private string ValidarCampos()
        {
            if (string.IsNullOrEmpty(txtEmail.Text)) return "Email vacío";
            if (string.IsNullOrEmpty(txtPassword.Text)) return "Contraseña vacía";
            return "";
        }
        #endregion

        
    }
}