﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using ProyectoNotas.Views.VistasColaboradores;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views.VistasConfiguraciones
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ColaboracionesRecibidas : ContentPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        public ColaboracionesRecibidas()
        {
            InitializeComponent();
            //lstColaboradores.RefreshCommand = new Command(() => {
            //    lstColaboradores.IsRefreshing = true;
            LlenarListado();
            //    lstColaboradores.IsRefreshing = false;
            //});
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LlenarListado();
        }
        private void lstColaboradores_Refreshing(object sender, EventArgs e)
        {
            LlenarListado();
            lstColaboradores.IsRefreshing = false;
        }
        private void lstColaboradores_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        if (e.SelectedItem != null)
                        {
                            ColaboradoresModel objColaboracion = (ColaboradoresModel)e.SelectedItem;
                            //int IdColaboracion = Convert.ToInt32(objColaboracion.ID);
                            if (sender is ListView lv) lv.SelectedItem = null;

                            DestalleSolicitudRecibida objSolicitudRecibida = new DestalleSolicitudRecibida(objColaboracion);
                            objSolicitudRecibida.CloseWhenBackgroundIsClicked = true;

                            await PopupNavigation.Instance.PushAsync(objSolicitudRecibida);

                        }
                    }

                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: ColaboracionesRecibidas.xaml.cs, Method: lstColaboradores_ItemSelected, Error: " + ex.Message);

            }
        }
        private void LlenarListado()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        ColaboradoresAdapter objColaboradoresAdapter = new ColaboradoresAdapter();
                        UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        List<ColaboradoresModel> listColaboradores = new List<ColaboradoresModel>();
                        listColaboradores = objColaboradoresAdapter.ObtenerSolicitudColaborarRecibida(UserLogged.ID);
                        lstColaboradores.ItemsSource = listColaboradores;
                        if (listColaboradores.Count == 0)
                        {
                            lblNoTareas.IsVisible = true;
                        }
                        else
                        {
                            lblNoTareas.IsVisible = false;
                        }
                    }
                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: ColaboracionesRecibidas.xaml.cs, Method: LlenarListado, Error: " + ex.Message);

            }
        }

      
    }
}