﻿using Acr.UserDialogs;
using ProyectoNotas.Models;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views.VistasConfiguraciones
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Configuraciones : ContentPage
    {
        public ObservableCollection<MenuPrincipalFlyoutMenuItem> MenuItems { get; set; }

        public Configuraciones()
        {
            InitializeComponent();
            MenuItems = new ObservableCollection<MenuPrincipalFlyoutMenuItem>(new[]
                {
                    new MenuPrincipalFlyoutMenuItem { Id = 0, Title = "Sección Colaboraciones", Foto="" , EsVisible="False" },
                    new MenuPrincipalFlyoutMenuItem { Id = 1, Title = "Solicitudes Enviadas", Foto="iconSiguiente", EsVisible="True" },
                    new MenuPrincipalFlyoutMenuItem { Id = 2, Title = "Solicitudes Recibidas", Foto="iconSiguiente", EsVisible="True"  },
                    new MenuPrincipalFlyoutMenuItem { Id = 3, Title = "Sección Info App", Foto="" , EsVisible="False" },
                    new MenuPrincipalFlyoutMenuItem { Id = 4, Title = "Acerca de SimpleLife", Foto="iconSiguiente", EsVisible="True"  },
                });
            lstConfiguracion.ItemsSource = MenuItems;

        }

        private void lstConfiguracion_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                using (UserDialogs.Instance.Loading(("Cargando...")))
                {
                    await Task.Delay(300);
                    var item = e.SelectedItem as MenuPrincipalFlyoutMenuItem;
                    if (item == null)
                    {
                        return;
                    }

                    if (item.Id == 1)
                    {
                        await Navigation.PushAsync(new ColaboracionesEnviadas());
                    }
                    else if (item.Id == 2)
                    {
                        await Navigation.PushAsync(new ColaboracionesRecibidas());
                    }
                    else if (item.Id == 4)
                    {
                        await Navigation.PushAsync(new AcercaDe());
                    }
                }
            });
        }

       

    }
}
