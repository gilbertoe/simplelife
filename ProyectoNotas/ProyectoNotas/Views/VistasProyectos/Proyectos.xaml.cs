﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views.VistasProyectos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Proyectos : ContentPage
    {

        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        public Proyectos()
        {
            InitializeComponent();
            //lstProyectos.RefreshCommand = new Command(() =>
            //{
            //    lstProyectos.IsRefreshing = true;
            LlenarListado(0);
            //    lstProyectos.IsRefreshing = false;
            //});
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            LlenarListado(smgFiltrarProyectos.SelectedSegment);
        }
        private void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            CrearEditarProyecto objVistaProyecto = new CrearEditarProyecto();
            objVistaProyecto.CloseWhenBackgroundIsClicked = true;
            PopupNavigation.Instance.PushAsync(objVistaProyecto);
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            EliminarProyecto(sender);
        }
        private void lstProyectos_Refreshing(object sender, EventArgs e)
        {
            LlenarListado(0);
            lstProyectos.IsRefreshing = false;
        }
        private void lstProyectos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        if (e.SelectedItem != null)
                        {
                            ProyectosModel objProyecto = (ProyectosModel)e.SelectedItem;
                            if (sender is ListView lv) lv.SelectedItem = null;
                            await Navigation.PushAsync(new DetalleProyecto(objProyecto));
                        }
                    }
                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: Proyectos.xaml.cs, Method: lstProyectos_ItemSelected, Error: " + ex.Message);

            }
        }
        private void smgFiltrarProyectos_OnSegmentSelected(object sender, Plugin.Segmented.Event.SegmentSelectEventArgs e)
        {
            this.LlenarListado(smgFiltrarProyectos.SelectedSegment);
        }
        private void EliminarProyecto(object sender)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        MenuItem viewCellSelected = sender as MenuItem;
                        ProyectosModel ProyectoEliminar = viewCellSelected?.BindingContext as ProyectosModel;
                        ProyectosAdapters objProyectosAdapter = new ProyectosAdapters();
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        if (objProyectosAdapter.EliminarProyecto(ProyectoEliminar.ID))
                        {
                            await DisplayAlert("Notificación", "Proyecto Eliminado con éxito", "OK");
                            LlenarListado(0);

                        }
                        else
                        {
                            await DisplayAlert("Intenta más tarde ", "Ocurrió un fallo al intentar eliminar el proyecto", "OK");
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: Proyectos.xaml.cs, Method: EliminarProyecto, Error: " + ex.Message);
            }

        }

        private void LlenarListado(int Filtro)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        ProyectosAdapters objProyectosAdapter = new ProyectosAdapters();
                        UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        List<ProyectosModel> listProyectos = new List<ProyectosModel>();
                        switch (Filtro)
                        {
                            case 0:
                                listProyectos = objProyectosAdapter.ExtraerProyectosUsuario(UserLogged.ID);
                                break;
                            case 1:
                                listProyectos = objProyectosAdapter.ExtraerProyectosCompartidos(UserLogged.ID);
                                break;
                            case 2:
                                listProyectos = objProyectosAdapter.ExtraerProyectosInvitado(UserLogged.ID);
                                break;

                        }
                        lstProyectos.ItemsSource = listProyectos;
                        if (listProyectos.Count == 0)
                        {
                            lblNoTareas.IsVisible = true;
                        }
                        else
                        {
                            lblNoTareas.IsVisible = false;
                        }
                    }
                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: Proyectos.xaml.cs, Method: LlenarListado, Error: " + ex.Message);

            }
        }

       
    }
}