﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using ProyectoNotas.Views.VistasTareas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views.VistasProyectos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetalleProyecto : ContentPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        private int IdProyectoLocal = 0;
        private int IdUsuarioLocal = 0;
        public DetalleProyecto(ProyectosModel objProyecto)
        {
            InitializeComponent();
            IdProyectoLocal = objProyecto.ID;
            IdUsuarioLocal = objProyecto.IdUsuario;
            txtDescripcion.Text = objProyecto.Descripcion;
            txtNombreProyecto.Text = objProyecto.Nombre;
            LlenarListado(objProyecto.ID);
            //lstTareas.RefreshCommand = new Command(() =>
            //{
            //    lstTareas.IsRefreshing = true;
            //    LlenarListado(objProyecto.ID);
            //    lstTareas.IsRefreshing = false;
            //});
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LlenarListado(IdProyectoLocal);
        }
        private void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                using (UserDialogs.Instance.Loading(("Cargando...")))
                {
                    await Task.Delay(300);
                    await Navigation.PushAsync(new CrearEditarTarea(IdProyecto: IdProyectoLocal));
                }
            });

        }
        private void lstTareas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {

                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        if (e.SelectedItem != null)
                        {
                            TareasModel objTarea = (TareasModel)e.SelectedItem;
                            int IdTarea = Convert.ToInt32(objTarea.ID);
                            if (sender is ListView lv) lv.SelectedItem = null;
                            await Navigation.PushAsync(new CrearEditarTarea(IdTarea: IdTarea, IdProyecto: IdProyectoLocal));
                        }
                    }

                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: DetalleProyecto.xaml.cs, Method: lstTareas_ItemSelected, Error: " + ex.Message);

            }
        }
        private void lstTareas_Refreshing(object sender, EventArgs e)
        {
            LlenarListado(IdProyectoLocal);
            lstTareas.IsRefreshing = false;
        }
        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            this.EliminarTarea(sender);
        }

        private void btnGuardar_Clicked(object sender, EventArgs e)
        {
            GuardarProyecto();
        }

        private void LlenarListado(int IdProyecto)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        TareasAdapter objTareasAdapter = new TareasAdapter();
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        lstTareas.ItemsSource = objTareasAdapter.ExtraerTareasProyecto(IdProyecto);
                    }
                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: Notas.xaml.cs, Method: LlenarListado, Error: " + ex.Message);

            }
        }

        private void EliminarTarea(object sender)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        MenuItem viewCellSelected = sender as MenuItem;
                        TareasModel NotaEliminar = viewCellSelected?.BindingContext as TareasModel;
                        TareasAdapter objTareasAdapter = new TareasAdapter();
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        if (objTareasAdapter.EliminarTareaProyecto(NotaEliminar.ID))
                        {
                            await DisplayAlert("Notificación", "Tarea removida del proyecto", "OK");
                            LlenarListado(IdProyectoLocal);

                        }
                        else
                        {
                            await DisplayAlert("Intenta más tarde ", "Ocurrió un fallo al intentar eliminar la tarea", "OK");
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: Notas.xaml.cs, Method: EliminarTarea, Error: " + ex.Message);
            }

        }

        private void GuardarProyecto()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        string MensajeCampoVacio = ValidarCampos();
                        if (!string.IsNullOrEmpty(MensajeCampoVacio))
                        {
                            await DisplayAlert("Notificación", MensajeCampoVacio, "OK");
                            return;
                        }

                        ProyectosModel NuevoProyecto = new ProyectosModel();
                        NuevoProyecto.IdUsuario = IdUsuarioLocal;
                        NuevoProyecto.Nombre = txtNombreProyecto.Text;
                        NuevoProyecto.Descripcion = txtDescripcion.Text;
                        NuevoProyecto.ID = IdProyectoLocal;
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        ProyectosAdapters objProyectosAdapter = new ProyectosAdapters();
                        if (objProyectosAdapter.GuardarProyecto(NuevoProyecto))
                        {

                            await DisplayAlert("Notificación", "Proyecto guardado con éxito", "OK");
                            //await PopupNavigation.Instance.PopAsync();
                        }
                        else
                        {
                            await DisplayAlert("Por favor intente más tarde", "Ocurrió un erro al intentar guardar el proyecto", "OK");
                        }
                    }
                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: CrearEditarProyecto, Método: GuardarProyecto, Error: " + ex.Message);

            }
        }
        private string ValidarCampos()
        {
            if (string.IsNullOrEmpty(txtNombreProyecto.Text))
            {
                return "Nombre Vacío";
            }

            return "";
        }

       
    }
}