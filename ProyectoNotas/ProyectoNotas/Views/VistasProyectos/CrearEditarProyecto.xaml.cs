﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views.VistasProyectos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CrearEditarProyecto : PopupPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        private int IdProyectoLocal = 0;

        public CrearEditarProyecto(ProyectosModel objProyecto = null)
        {
            InitializeComponent();
            if (objProyecto != null)
            {
                this.Title = "Editar Proyecto";
                LlenarDatos(objProyecto);

            }
            else
            {
                this.Title = "Crear Proyecto";
            }
        }

        //private void btnCerrar_Clicked(object sender, EventArgs e)
        //{
        //    PopupNavigation.Instance.PopAsync();
        //}

        private void btnGuardar_Clicked(object sender, EventArgs e)
        {
            GuardarProyecto();
        }

        private void LlenarDatos(ProyectosModel objProyecto)
        {
            try
            {
                IdProyectoLocal = objProyecto.ID;
                txtNombreProyecto.Text = objProyecto.Nombre;
                txtDescripcion.Text = objProyecto.Descripcion;
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: CrearEditarProyecto, Método: LlenarDatos, Error: " + ex.Message);

            }
        }

        private void GuardarProyecto()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        string MensajeCampoVacio = ValidarCampos();
                        if (!string.IsNullOrEmpty(MensajeCampoVacio))
                        {
                            await DisplayAlert("Notificación", MensajeCampoVacio, "OK");
                            return;
                        }

                        UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
                        ProyectosModel NuevoProyecto = new ProyectosModel();
                        NuevoProyecto.IdUsuario = UserLogged.ID;
                        NuevoProyecto.Nombre = txtNombreProyecto.Text;
                        NuevoProyecto.Descripcion = txtDescripcion.Text;


                        if (IdProyectoLocal > 0)
                        {
                            NuevoProyecto.ID = IdProyectoLocal;
                        }
                        else
                        {
                            NuevoProyecto.ID = 0;
                        }
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        ProyectosAdapters objProyectosAdapter = new ProyectosAdapters();
                        if (objProyectosAdapter.GuardarProyecto(NuevoProyecto))
                        {
                            if (IdProyectoLocal > 0)
                            {
                                await DisplayAlert("Notificación", "Proyecto guardado con éxito", "OK");
                            }
                            else
                            {
                                await DisplayAlert("Notificación", "Proyecto creado con éxito", "OK");
                            }
                            await PopupNavigation.Instance.PopAsync();
                        }
                        else
                        {
                            await DisplayAlert("Por favor intente más tarde", "Ocurrió un erro al intentar guardar el proyecto", "OK");
                        }

                    }
                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: CrearEditarProyecto, Método: GuardarProyecto, Error: " + ex.Message);

            }
        }
        private string ValidarCampos()
        {
            if (string.IsNullOrEmpty(txtNombreProyecto.Text))
            {
                return "Nombre Vacío";
            }

            return "";
        }
       
    }
}