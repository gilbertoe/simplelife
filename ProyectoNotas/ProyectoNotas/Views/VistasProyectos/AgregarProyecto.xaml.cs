﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views.VistasProyectos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarProyecto : ContentPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        private int IdTareaLocal = 0;
        public AgregarProyecto(int IdTarea = 0)
        {
            InitializeComponent();
            IdTareaLocal = IdTarea;
            //lstProyectos.RefreshCommand = new Command(() =>
            //{
            //    lstProyectos.IsRefreshing = true;
            LlenarListado();
            //    lstProyectos.IsRefreshing = false;
            //});
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            LlenarListado();
        }
        private void lstProyectos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                ProyectosModel objProyecto = (ProyectosModel)e.SelectedItem;
                if (sender is ListView lv) lv.SelectedItem = null;
                SeleccionarProyecto(objProyecto.ID);
            }
        }
        private void lstProyectos_Refreshing(object sender, EventArgs e)
        {
            LlenarListado();
            lstProyectos.IsRefreshing = false;
        }
        private void SeleccionarProyecto(int IdProyecto)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                var answer = await DisplayAlert("Agregar proyecto", "Quieres agregar tu tarea a este proyecto?", "Si", "No");
                if (answer)
                {

                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        ProyectosAdapters objProyectosAdapter = new ProyectosAdapters();
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        if (objProyectosAdapter.AgregarTareaProyecto(IdTareaLocal, IdProyecto))
                        {
                            await DisplayAlert("Notificación", "Tarea Agregada al proyecto", "OK");
                        }
                    }
                }
            });
        }
        private void LlenarListado()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        ProyectosAdapters objProyectosAdapter = new ProyectosAdapters();
                        UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        lstProyectos.ItemsSource = objProyectosAdapter.ExtraerProyectosUsuario(UserLogged.ID);
                    }
                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: Proyectos.xaml.cs, Method: LlenarListado, Error: " + ex.Message);

            }
        }

      
    }
}