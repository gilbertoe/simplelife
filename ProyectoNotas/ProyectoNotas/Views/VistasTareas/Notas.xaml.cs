﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using ProyectoNotas.Persistence;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views.VistasTareas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Notas : ContentPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        //PRUEBAS
        //private SQLiteAsyncConnection sqliteConnection;
        private List<TareasModel> listTareas = new List<TareasModel>();

        public Notas()
        {
            InitializeComponent();
            //PRUEBAS
            //sqliteConnection = DependencyService.Get<ISQLiteDb>().GetConnection();
            //Device.BeginInvokeOnMainThread(async () =>
            //{
            //    var Tareas = await sqliteConnection.Table<TareasModelSQLite>().ToListAsync();
            //    _tareas = new ObservableCollection<TareasModelSQLite>(Tareas);
            //    lstTareas.ItemsSource = _tareas;
            //});
            //lstTareas.RefreshCommand = new Command(() =>
            //{
            //    lstTareas.IsRefreshing = true;
            LlenarListado(smgFiltrarTareas.SelectedSegment + 1);
            //    lstTareas.IsRefreshing = false;
            //});

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            LlenarListado(smgFiltrarTareas.SelectedSegment + 1);
        }
        private void lstTareas_Refreshing(object sender, EventArgs e)
        {
            LlenarListado(smgFiltrarTareas.SelectedSegment + 1);
            lstTareas.IsRefreshing = false;
        }
        #region Events
        private void lstTareas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);

                        if (e.SelectedItem != null)
                        {
                            TareasModel objTarea = (TareasModel)e.SelectedItem;
                            int IdTarea = Convert.ToInt32(objTarea.ID);
                            if (sender is ListView lv) lv.SelectedItem = null;
                            await Navigation.PushAsync(new CrearEditarTarea(IdTarea: IdTarea));
                        }
                    }
                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: Notas.xaml.cs, Method: lstTareas_ItemSelected, Error: " + ex.Message);

            }
        }
        private void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                using (UserDialogs.Instance.Loading(("Cargando...")))
                {
                    await Task.Delay(300);
                    await Navigation.PushAsync(new CrearEditarTarea());
                }
            });
        }
        private void smgFiltrarTareas_OnSegmentSelected(object sender, Plugin.Segmented.Event.SegmentSelectEventArgs e)
        {
            this.LlenarListado(smgFiltrarTareas.SelectedSegment + 1);
        }
        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            this.EliminarTarea(sender);
        }
        #endregion
        #region Metodos
        private void EliminarTarea(object sender)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        MenuItem viewCellSelected = sender as MenuItem;
                        TareasModel NotaEliminar = viewCellSelected?.BindingContext as TareasModel;
                        TareasAdapter objTareasAdapter = new TareasAdapter();
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        if (objTareasAdapter.EliminarTarea(NotaEliminar.ID))
                        {
                            await DisplayAlert("Notificación", "Tarea Eliminada con éxito", "OK");
                            LlenarListado(smgFiltrarTareas.SelectedSegment + 1);

                        }
                        else
                        {
                            await DisplayAlert("Intenta más tarde ", "Ocurrió un fallo al intentar eliminar la tarea", "OK");
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: Notas.xaml.cs, Method: EliminarTarea, Error: " + ex.Message);
            }
           
        }
        private void LlenarListado(int IdStatus)
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        TareasAdapter objTareasAdapter = new TareasAdapter();
                        UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        listTareas =  objTareasAdapter.ExtraerTareasUsuario(UserLogged.ID, IdStatus);
                        lstTareas.ItemsSource = listTareas;
                        if (listTareas.Count == 0)
                        {
                            lblNoTareas.IsVisible = true;
                        }
                        else
                        {
                            lblNoTareas.IsVisible = false;
                        }
                    }
                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Class: Notas.xaml.cs, Method: LlenarListado, Error: " + ex.Message);

            }
        }
        #endregion

       
    }
}