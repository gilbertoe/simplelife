﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Plugin.Permissions;
using ProyectoNotas.Adapters;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using ProyectoNotas.Persistence;
using ProyectoNotas.Services;
using ProyectoNotas.Views.VistasProyectos;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoNotas.Views.VistasTareas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CrearEditarTarea : ContentPage
    {
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        //private SQLiteAsyncConnection sqliteConnection;

        private int IdTareaLocal = 0;
        private int IdProyectoLocal = 0;
        private int EstatusSeleccionado = 0;
        private int PrioridadSeleccionada = 0;

        public CrearEditarTarea(int IdTarea = 0, int IdProyecto = 0)
        {
            InitializeComponent();

            //sqliteConnection = DependencyService.Get<ISQLiteDb>().GetConnection();

            Device.BeginInvokeOnMainThread(async () =>
            {

                if (IdTarea > 0)
                {

                    IdProyectoLocal = IdProyecto;
                    this.Title = "Editar Tarea";
                    this.pckProyectos.IsVisible = true;
                    this.pckProyectos.IsEnabled = true;
                    await LlenarDatos(IdTarea);
                }
                else
                {

                    this.Title = "Crear Tarea";
                    this.btnTerminarTarea.IsEnabled = false;
                    this.btnTerminarTarea.IsVisible = false;
                    this.lblTerminarTarea.IsVisible = false;
                    this.pckProyectos.IsVisible = false;
                    this.pckProyectos.IsEnabled = false;
                    this.lblSeleccionarProyecto.IsVisible = false;
                   
                }

            });
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
        #region Events
        private void btnTerminarTarea_Clicked(object sender, EventArgs e)
        {
            ActualizarEstatusTarea();
        }
        private void btnProyectos_Clicked(object sender, EventArgs e)
        {
             Navigation.PushAsync(new AgregarProyecto(IdTareaLocal));

        }
        private void smgEstatusTarea_OnSegmentSelected(object sender, Plugin.Segmented.Event.SegmentSelectEventArgs e)
        {
            this.EstatusSeleccionado = smgEstatusTarea.SelectedSegment + 1;
        }
        private void smgPrioridadTarea_OnSegmentSelected(object sender, Plugin.Segmented.Event.SegmentSelectEventArgs e)
        {
            this.PrioridadSeleccionada = smgPrioridadTarea.SelectedSegment + 1;
        }
        private void btnGuardad_Clicked(object sender, EventArgs e)
        {
            this.GuardarTarea();
        }
        #endregion
        #region Metodos
        private async Task LlenarDatos(int IdTarea)
        {
            try
            {

                IdTareaLocal = IdTarea;
                TareasAdapter objTareasAdapter = new TareasAdapter();
                if (!generalHelper.ValidarConexionInternet())
                {
                    await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                    return;
                }
                TareasModel objTarea = objTareasAdapter.TraerDetalleTarea(IdTarea);
                txtTitulo.Text = objTarea.Titulo;
                txtDescripcion.Text = objTarea.Descripcion;
                dtmFechaInicio.Date = objTarea.FechaInicio.Date;
                dtmHoraInicio.Time = objTarea.FechaInicio.TimeOfDay;
                dtmFechaFin.Date = objTarea.FechaFin.Date;
                dtmHoraFin.Time = objTarea.FechaFin.TimeOfDay;
                smgEstatusTarea.SelectedSegment = objTarea.IdEstatus - 1;
                smgPrioridadTarea.SelectedSegment = objTarea.IdPrioridad - 1;
                LlenarPickers(objTarea.ID, objTarea.IdProyecto);
                if (objTarea.IdEstatus == 3)
                {
                    lblTerminarTarea.IsVisible = false;
                    btnTerminarTarea.IsEnabled = false;
                    btnTerminarTarea.IsVisible = false;
                }

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: CrearEditarTarea, Método: LlenarDatos, Error: " + ex.Message);

            }
        }
        private void GuardarTarea()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        string MensajeCampoVacio = ValidarCampos();
                        if (!string.IsNullOrEmpty(MensajeCampoVacio))
                        {
                            await DisplayAlert("Notificación", MensajeCampoVacio, "OK");
                            return;
                        }
                        if ((dtmFechaInicio.Date + dtmHoraInicio.Time) >= (dtmFechaFin.Date + dtmHoraFin.Time))
                        {
                            await DisplayAlert("Notificación", "La fecha de inicio debe de ser menor a la fecha fin", "OK");
                            return;
                        }
                        UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
                        TareasModel NuevaTarea = new TareasModel();
                        NuevaTarea.IdUsuario = UserLogged.ID;
                        NuevaTarea.Titulo = txtTitulo.Text;
                        NuevaTarea.Descripcion = txtDescripcion.Text;
                        NuevaTarea.FechaInicio = dtmFechaInicio.Date + dtmHoraInicio.Time;
                        NuevaTarea.FechaFin = dtmFechaFin.Date + dtmHoraFin.Time;

                        if (EstatusSeleccionado == 0)
                        {
                            NuevaTarea.IdEstatus = 1;
                        }
                        else
                        {
                            NuevaTarea.IdEstatus = EstatusSeleccionado;
                        }
                        if (PrioridadSeleccionada == 0)
                        {
                            NuevaTarea.IdPrioridad = 1;
                        }
                        else
                        {
                            NuevaTarea.IdPrioridad = PrioridadSeleccionada;
                        }

                        if (IdTareaLocal > 0)
                        {
                            NuevaTarea.ID = IdTareaLocal;
                        }
                        else
                        {
                            NuevaTarea.ID = 0;
                        }
                        if (!generalHelper.ValidarConexionInternet())
                        {
                            await DisplayAlert("Notificación", "No tienes conexión a internet, verifica tu conexión", "OK");
                            return;
                        }
                        TareasAdapter objTareasAdapter = new TareasAdapter();

                        if (this.pckProyectos.SelectedIndex >= 0)
                        {
                            NuevaTarea.IdProyecto = ((ProyectosModel)pckProyectos.SelectedItem).ID;
                        }
                        else if (IdProyectoLocal > 0)
                        {
                            NuevaTarea.IdProyecto = IdProyectoLocal;
                        }
                        if (objTareasAdapter.GuardarTarea(NuevaTarea))
                        {
                            //TareasModelSQLite obtTarea = new TareasModelSQLite();

                            //obtTarea.Descripcion = NuevaTarea.Descripcion;
                            //obtTarea.FechaFin = NuevaTarea.FechaFin;
                            //obtTarea.FechaInicio = NuevaTarea.FechaInicio;
                            //obtTarea.IdEstatus = NuevaTarea.IdEstatus;
                            //obtTarea.IdPrioridad = NuevaTarea.IdPrioridad;
                            //obtTarea.IdProyecto = NuevaTarea.IdProyecto;
                            //obtTarea.IdUsuario = NuevaTarea.IdUsuario;
                            //obtTarea.Titulo = NuevaTarea.Titulo;
                            if (IdTareaLocal > 0)
                            {
                                //obtTarea.ID = NuevaTarea.ID;
                                //await sqliteConnection.UpdateAsync(obtTarea);
                                await DisplayAlert("Notificación", "Tarea guardada con éxito", "OK");
                            }
                            else
                            {
                                //await sqliteConnection.InsertAsync(obtTarea);
                                await DisplayAlert("Notificación", "Tarea creada con éxito", "OK");

                                /////////PUEBAS CALENDARIO
                                
                                var action = await DisplayAlert("Guardar en calendario", "¿Desea registrar esta tarea en el calendario?", "Si", "No");
                                if (action)
                                {
                                    Plugin.Permissions.Abstractions.PermissionStatus status = await CrossPermissions.Current.CheckPermissionStatusAsync<CalendarPermission>();
                                    if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                                    {
                                        PermissionStatus permissionStatus = (PermissionStatus)await CrossPermissions.Current.RequestPermissionAsync<CalendarPermission>();
                                        if (permissionStatus != PermissionStatus.Granted)
                                        {

                                            await DisplayAlert("Notificación", "No tienes permisos al calendario", "OK");
                                        }
                                        else
                                        {
                                            var eventDetails = await DependencyService.Get<ICalendarService>().AddEventToCalendar(NuevaTarea.FechaInicio, NuevaTarea.FechaFin,
                                           NuevaTarea.Titulo, NuevaTarea.Descripcion, "Casa");
                                        }
                                    }
                                    else
                                    {
                                        var eventDetails = await DependencyService.Get<ICalendarService>().AddEventToCalendar(NuevaTarea.FechaInicio, NuevaTarea.FechaFin,
                                       NuevaTarea.Titulo, NuevaTarea.Descripcion, "Casa");
                                    }
                                }

                                /////////FIN DE PRUEBAS
                            }

                            await Navigation.PopAsync();
                        }
                        else
                        {
                            await DisplayAlert("Por favor intente más tarde", "Ocurrió un erro al intentar guardar la tarea", "OK");
                        }

                    }
                });

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: CrearEditarTarea, Método: GuardarTarea, Error: " + ex.Message);

            }
        }
        private void ActualizarEstatusTarea()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        if (IdTareaLocal > 0)
                        {
                            TareasAdapter objTareasAdapter = new TareasAdapter();
                            bool Resultado = objTareasAdapter.ActualizarEstatusTarea(IdTareaLocal, 3);
                            if (Resultado)
                            {
                                btnTerminarTarea.ImageSource = "iconTerminado";
                                await DisplayAlert("Notificación", "Tarea terminada!", "OK");
                                await Navigation.PopAsync();
                            }
                            else
                            {
                                await DisplayAlert("Tarea no actualizada", "Por favor intentan más tarde", "OK");
                            }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: CrearEditarTarea, Método: ActualizarEstatusTarea, Error: " + ex.Message);
            }
        }
        private string ValidarCampos()
        {
            if (string.IsNullOrEmpty(txtTitulo.Text))
            {
                return "Título Vacío";
            }

            return "";
        }
        private void LlenarPickers(int IdTarea = 0, int idProyecto = 0)
        {
            try
            {

                ProyectosAdapters objProyectosAdapter = new ProyectosAdapters();
                PrioridadTareaAdapter objPrioridadTareaAdapter = new PrioridadTareaAdapter();
                UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));

                List<ProyectosModel> lstProyectos = objProyectosAdapter.ExtraerProyectosUsuario(UserLogged.ID);
                pckProyectos.ItemsSource = lstProyectos;

                if (IdTarea > 0)
                {
                    pckProyectos.SelectedItem = lstProyectos.Find(x => x.ID == idProyecto);
                }

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: CrearEditarTarea, Método: LlenarPickers, Error: " + ex.Message);

            }
        }
        #endregion
    }
}