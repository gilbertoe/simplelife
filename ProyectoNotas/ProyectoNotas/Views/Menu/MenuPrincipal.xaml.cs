﻿using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using ProyectoNotas.Views.VistasConfiguraciones;
using ProyectoNotas.Views.VistasColaboradores;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProyectoNotas.Views.VistasProyectos;
using ProyectoNotas.Views.VistasTareas;

namespace ProyectoNotas.Views.Menu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPrincipal : FlyoutPage
    {
        private MailingHelper mailinghelper = new MailingHelper();

        public MenuPrincipal()
        {
            InitializeComponent();
            FlyoutPage.ListView.ItemSelected += ListView_ItemSelected;
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MenuPrincipalFlyoutMenuItem;
            if (item == null)
                return;
            if (item.Id == 0)
            {
                Detail = new NavigationPage(new Notas());
            }
            else if (item.Id == 1)
            {
                Detail = new NavigationPage(new Proyectos());
            }
            else if (item.Id == 2)
            {
                Detail = new NavigationPage(new Colaboradores());
            }
            else if (item.Id == 3)
            {
                Detail = new NavigationPage(new Configuraciones());
            }
            else if (item.Id == 4)
            {
                try
                {
                    NavigateToLoginAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
               
            }
            IsPresented = false;
            FlyoutPage.ListView.SelectedItem = null;
        }

        private async void NavigateToLoginAsync()
        {
            try
            {
                var action = await DisplayAlert("Cerrar sesión", "¿Estas seguro que quieres salir?", "Si", "No");
                if (action)
                {
                    Preferences.Clear();
                    Application.Current.MainPage = new NavigationPage(new Login());
                    await Navigation.PopAsync();
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: MenuPrincipal, Método: NavigateToLoginAsync, Error: " + ex.Message);
            }
        }
    }
}