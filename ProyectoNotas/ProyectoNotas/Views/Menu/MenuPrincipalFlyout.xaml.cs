﻿using ProyectoNotas.Views;
using ProyectoNotas.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProyectoNotas.Helpers;
using ProyectoNotas.Adapters;
using System.IO;
using Acr.UserDialogs;

namespace ProyectoNotas.Views.Menu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPrincipalFlyout : ContentPage
    {
        public ListView ListView;
        private MailingHelper mailinghelper = new MailingHelper();
        private GeneralHelper generalHelper = new GeneralHelper();
        public MenuPrincipalFlyout()
        {
            InitializeComponent();

            BindingContext = new MenuPrincipalFlyoutViewModel();
            ListView = MenuItemsListView;
            this.ActualizarDatosPerfil();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.ActualizarDatosPerfil();
        }

        private void ActualizarDatosPerfil()
        {
            try
            {
                UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
                lblNombreUsuario.Text = UserLogged.Nombres;
                if (UserLogged.Foto != null)
                {
                    btnPerfil.Source = ImageSource.FromStream(() => new MemoryStream(UserLogged.Foto));
                    btnPerfil.Aspect = Aspect.AspectFill;
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: MenuPrincipalFlyout, Método: ActualizarDatosPerfil, Error: " + ex.Message);
            }

        }

        class MenuPrincipalFlyoutViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<MenuPrincipalFlyoutMenuItem> MenuItems { get; set; }

            public MenuPrincipalFlyoutViewModel()
            {
                MenuItems = new ObservableCollection<MenuPrincipalFlyoutMenuItem>(new[]
                {
                    new MenuPrincipalFlyoutMenuItem { Id = 0, Title = "Mis Tareas", Foto="iconNotes" },
                    new MenuPrincipalFlyoutMenuItem { Id = 1, Title = "Mis Proyectos", Foto="iconProyectos" },
                    new MenuPrincipalFlyoutMenuItem { Id = 2, Title = "Mis Colaboradores", Foto="iconColaboradores" },
                    new MenuPrincipalFlyoutMenuItem { Id = 3, Title = "Configuraciones", Foto="Config" },
                    new MenuPrincipalFlyoutMenuItem { Id = 4, Title = "Cerrar Sesión", Foto="IconExit" },
                });
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }

        private void btnPerfil_Clicked(object sender, EventArgs e)
        {

           NavigateAsync();
        }

        private void NavigateAsync()
        {
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    using (UserDialogs.Instance.Loading(("Cargando...")))
                    {
                        await Task.Delay(300);
                        await Navigation.PushModalAsync(new Perfil(true));
                    }
                });
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: MenuPrincipalFlyout, Método: NavigateAsync, Error: " + ex.Message);
            }
        }

       

    }
}