﻿using ProyectoNotas.Views;
using ProyectoNotas.Views.Menu;
using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.LocalNotification;
using ProyectoNotas.Adapters;
using Newtonsoft.Json;
using ProyectoNotas.Models;
using System.Collections.Generic;
using ProyectoNotas.Persistence;
using SQLite;

namespace ProyectoNotas
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            //SQLiteAsyncConnection sqliteConnection = DependencyService.Get<ISQLiteDb>().GetConnection();
            //sqliteConnection.CreateTableAsync<TareasModelSQLite>();

            if (Preferences.ContainsKey("UserLogged"))
            {
                MainPage = new MenuPrincipal();
            }
            else
            {
                MainPage = new NavigationPage(new Login());

            }
        }

        protected override void OnStart()
        {
            base.OnStart();
            SendNotificationNow();
        }

        protected override void OnSleep()
        {
            base.OnSleep();
            //SendNotificationNow();
        }

        protected override void OnResume()
        {
            base.OnResume();
            //SendNotificationNow();

        }
        private void SendNotificationNow()
        {

            Device.StartTimer(TimeSpan.FromMilliseconds(240000), () =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (Preferences.ContainsKey("UserLogged"))
                    {
                        try
                        {
                            NotificacionesAdapter objNotificaciones = new NotificacionesAdapter();
                            UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
                            List<TareasModel> NotificacionesPendientes = objNotificaciones.TraerProximasNotas(UserLogged.ID);
                            foreach (TareasModel Notificacion in NotificacionesPendientes)
                            {
                                await NotificationCenter.Current.Show((notification) => notification.WithScheduleOptions((schedule) => schedule.Build()).WithAndroidOptions((android) => android.WithPriority(NotificationPriority.High).Build()).WithiOSOptions((ios) => ios.Build()).WithTitle(string.Concat(Notificacion.Titulo, " está por comenzar")).WithDescription(string.Concat("Descripción: ", Notificacion.Descripcion)).Create());
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                    }


                });
                return true;
            });
        }
    }
}
