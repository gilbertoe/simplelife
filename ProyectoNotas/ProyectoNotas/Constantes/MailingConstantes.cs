﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoNotas.Constantes
{
    public class MailingConstantes
    {
        /// <summary>
        /// Dirección de email del emisor del mensaje
        /// </summary>
        public const string MAILING_EMISOR = "CC3Vp9S4pEURJhM+BvfJseBDBs2S4IG65zSt5l9abzE=";

        /// <summary>
        /// Dirección de email a la cual se desea enviar copia del mensaje
        /// </summary>
        public const string MAILING_COPIA = "gilberto.e.barrera@gmail.com";

        /// <summary>
        /// Motivo del mensaje de email
        /// </summary>
        public const string MAILING_MOTIVO = "¡Tienes un nuevo correo! ";

        /// <summary>
        /// Host que provee el servicio de email
        /// </summary>
        public const string MAILING_HOST = "smtp.gmail.com";

        /// <summary>
        /// Dirección de email del receptor en caso de que se presente un error
        /// de aplicación
        /// </summary>
        public const string MAILING_RECEPTOR_ERROR = "CC3Vp9S4pEURJhM+BvfJsY+x4Wl13C+qZJzwSnhm1xM=";

        /// <summary>
        /// Motivo de email del mensaje de error
        /// </summary>
        public const string MAILING_MOTIVO_ERROR = "Error de aplicación - NOTAS";

        /// <summary>
        /// Motivo de email el cual se envia al usuario para comprobar su correo cuando se registra
        /// </summary>
        public const string MAILING_ASUNTO_REGISTRO = "Correo de verificación - NOTAS";

        /// <summary>
        /// Cuerpo de email el cual se envia al usuario para comprobar su correo cuando se registra
        /// </summary>
        public const string MAILING_BODY_REGISTRO = "";

        /// <summary>
        /// Mensaje que aparece para el usuario en caso de error
        /// </summary>
        public const string MENSAJE_ERROR = "Hubo un error al enviar tu mensaje, intenta mas tarde.";

        public const string USER = "CC3Vp9S4pEURJhM+BvfJseBDBs2S4IG65zSt5l9abzE=";

        public const string PASSWORDGMAIL = "q53BxWOmI4g7sg0jHMgYlQw/dYgiHAgFj2D/3lqIOmg=";
        public const string PASSWORDAWS = "Jh/ztE7saGcMJk/FfnwYyN5qpBJuM+GLfWNPgHAqccE=";
    }
}
