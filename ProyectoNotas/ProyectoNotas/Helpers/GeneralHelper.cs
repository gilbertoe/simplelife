﻿using ProyectoNotas.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace ProyectoNotas.Helpers
{
    public class GeneralHelper
    {
        public bool ValidarConexionInternet()
        {
            try
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet) return true;
                return false;
            }
            catch
            {
                return false;
            }
        }
        public bool ValidarEmail(string Email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(Email);
                return addr.Address == Email;
            }
            catch
            {
                return false;
            }
        }
        public bool ValidarContrasena(string Contrasena)
        {

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMinimum8Max12Chars = new Regex(@".{8,12}");

            bool isValidated = hasNumber.IsMatch(Contrasena) && hasUpperChar.IsMatch(Contrasena) && hasMinimum8Max12Chars.IsMatch(Contrasena);

            return isValidated;
        }
        public bool ValidarTelefono(string Telefono)
        {
            try
            {
                bool EsTelefono = Telefono.All(char.IsNumber);
                if (EsTelefono)
                {
                    int Longitud = Telefono.Length;
                    if (Longitud >= 10)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }
        public void GuardarSesionLocal(UsuariosModel UserLogged)
        {
            string strUserLoged = JsonConvert.SerializeObject(UserLogged);
            Preferences.Set("UserLogged", strUserLoged);
        }
        public UsuariosModel TraerSesionLocal()
        {
            UsuariosModel UserLogged = JsonConvert.DeserializeObject<UsuariosModel>(Preferences.Get("UserLogged", ""));
            return UserLogged;
        }

    }
}
