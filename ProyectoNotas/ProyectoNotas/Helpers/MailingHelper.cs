﻿using ProyectoNotas.Constantes;
using ProyectoNotas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;


namespace ProyectoNotas.Helpers
{
    public class MailingHelper
    {
        private SmtpClient client = new SmtpClient();

        private MailMessage msg = new MailMessage();

        private ShaHelper shaHelp = new ShaHelper();

        protected string Password = "G1l3sci9BB";

        public void EnviarMensajeError(string error)
        {
            try
            {
                msg.From = new MailAddress(shaHelp.Decrypt(MailingConstantes.MAILING_EMISOR, Password));
                msg.To.Add(new MailAddress(shaHelp.Decrypt(MailingConstantes.MAILING_RECEPTOR_ERROR, Password)));

                msg.Subject = MailingConstantes.MAILING_MOTIVO_ERROR;
                msg.Body = error;
                msg.IsBodyHtml = true;

                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(shaHelp.Decrypt(MailingConstantes.USER, Password), shaHelp.Decrypt(MailingConstantes.PASSWORDAWS, Password));
                // TO DO: se cambió al puerto 587, anteriormente estaba el puerto 25, se cambió por que en mobile funciona con el puerto 587
                client.Port = 587;
                client.Host = MailingConstantes.MAILING_HOST;
                client.Send(msg);
            }
            catch (Exception e)
            {
                Console.WriteLine("Clase: MailingHelper, Método: EnviarMensajeError, Error: " + e.Message);
            }
        }
        public void EnviarContrasena(UsuariosModel usuariosModel)
        {
            try
            {
                string ContrasenaDesEncriptada = shaHelp.Decrypt(usuariosModel.Contrasena, Password);
                msg.From = new MailAddress(shaHelp.Decrypt(MailingConstantes.MAILING_EMISOR, Password));
                msg.To.Add(new MailAddress(usuariosModel.Email));

                msg.Subject = "Código de confirmación ";
                msg.Body = "<html><body><H3>Hola" + " " + usuariosModel.Nombres + "</H3>" +
                            "<p>Esta es tu contraseña para acceder a la app Simple Life " + ContrasenaDesEncriptada + ". </p>" +
                            "</body></html>";

                msg.IsBodyHtml = true;

                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(shaHelp.Decrypt(MailingConstantes.USER, Password), shaHelp.Decrypt(MailingConstantes.PASSWORDAWS, Password));
                // TO DO: se cambió al puerto 587, anteriormente estaba el puerto 25, se cambió por que en mobile funciona con el puerto 587
                client.Port = 587;
                client.Host = MailingConstantes.MAILING_HOST;
                client.Send(msg);
            }
            catch (Exception e)
            {
                Console.WriteLine("Clase: MailingHelper, Método: EnviarContrasena, Error: " + e.Message);
            }
        }
        public bool EnviarInvitacionColaborar(ColaboradoresModel usuariosModel, string CorreoInvitador, string NombreProyecto)
        {
            try
            {
                msg.From = new MailAddress(shaHelp.Decrypt(MailingConstantes.MAILING_EMISOR, Password));
                msg.To.Add(new MailAddress(usuariosModel.Usuario.Email));

                msg.Subject = "Invitación a colaborar ";
                msg.Body = "<html><body><H3>Hola" + " " + usuariosModel.Usuario.Nombres + "</H3>" +
                            "<p>el usuario " + CorreoInvitador + " de la app Simple Life te esta invitando a colaborar en su proyecto:" + NombreProyecto +
                            ", Solo Ingresa este código desde la app: " + usuariosModel.CodigoColaboracion + " en Configuaciones/Solicitudes Recibidas y listo!, ya estaras colaborando :). </p>" +
                            "</body></html>";

                msg.IsBodyHtml = true;

                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(shaHelp.Decrypt(MailingConstantes.USER, Password), shaHelp.Decrypt(MailingConstantes.PASSWORDAWS, Password));
                // TO DO: se cambió al puerto 587, anteriormente estaba el puerto 25, se cambió por que en mobile funciona con el puerto 587
                client.Port = 587;
                client.Host = MailingConstantes.MAILING_HOST;
                client.Send(msg);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Clase: MailingHelper, Método: EnviarContrasena, Error: " + e.Message);
            }
            return false;
        }
        public bool EnviarRespuestaColaborar(ColaboradoresModel usuariosModel, string CorreoInvitador, string Respuesta)
        {
            try
            {
                msg.From = new MailAddress(shaHelp.Decrypt(MailingConstantes.MAILING_EMISOR, Password));
                msg.To.Add(new MailAddress(usuariosModel.Usuario.Email));

                msg.Subject = "Invitación a colaborar ";
                msg.Body = "<html><body><H3>Hola" + " " + usuariosModel.Usuario.Nombres + "</H3>" +
                            "<p>el usuario " + CorreoInvitador + " de la app Simple Life ha </p><H3>" + Respuesta + "</H3><p> colaborar en tu proyecto:" + usuariosModel.NombreProyecto + ". </p>" +
                            "</body></html>";
                msg.IsBodyHtml = true;

                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(shaHelp.Decrypt(MailingConstantes.USER, Password), shaHelp.Decrypt(MailingConstantes.PASSWORDAWS, Password));
                // TO DO: se cambió al puerto 587, anteriormente estaba el puerto 25, se cambió por que en mobile funciona con el puerto 587
                client.Port = 587;
                client.Host = MailingConstantes.MAILING_HOST;
                client.Send(msg);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Clase: MailingHelper, Método: EnviarContrasena, Error: " + e.Message);
            }
            return false;
        }
        public void EnviarCodigoConfirmacion(string CodigoConfirmacion, UsuariosModel usuariosModel)
        {
            try
            {
                msg.From = new MailAddress(shaHelp.Decrypt(MailingConstantes.MAILING_EMISOR, Password));
                msg.To.Add(new MailAddress(usuariosModel.Email));

                msg.Subject = "Código de confirmación";
                msg.Body = "<html><body><H3>Hola" + " " + usuariosModel.Nombres + "</H3>" +
                            "<p>Este es tu código de confirmación para acceder a la Simple Life " + CodigoConfirmacion + ". </p>" +
                            "<p>Te recordamos que tu correo es: " + usuariosModel.Email + ". </p>" +
                            "</body></html>";

                msg.IsBodyHtml = true;
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(shaHelp.Decrypt(MailingConstantes.USER, Password), shaHelp.Decrypt(MailingConstantes.PASSWORDAWS, Password));
                // TO DO: se cambió al puerto 587, anteriormente estaba el puerto 25, se cambió por que en mobile funciona con el puerto 587
                client.Port = 587;
                client.Host = MailingConstantes.MAILING_HOST;
                client.Send(msg);
            }
            catch (Exception e)
            {
                Console.WriteLine("Clase: MailingHelper, Método: EnviarCodigoConfirmacion, Error: " + e.Message);
            }
        }
    }
}
