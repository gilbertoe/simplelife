﻿using ProyectoNotas.Constantes;
using ProyectoNotas.Helpers;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoNotas.Conexiones
{
    public abstract class ConexionMySql
    {
        protected MySqlConnection conn;
        protected MySqlDataReader reader;
        protected MySqlCommand command;
        protected MySqlTransaction transaction;
        private ShaHelper shaHelp = new ShaHelper();
        private string Password = "G1l3sci9BB";

        protected ConexionMySql()
        {
            //Conexion a AWS
            try
            {
                conn = new MySqlConnection(shaHelp.Decrypt(ConstantesConexion.ConexionAWS, Password));
            }
            catch
            {
                throw;
            }
        }

        protected MySqlCommand CreateCommand(string query) => new MySqlCommand(query, conn);

        protected MySqlCommand CreateCommand() => new MySqlCommand();
    }
}
