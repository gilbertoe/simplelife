﻿using SQLite;

namespace ProyectoNotas.Persistence
{
    public interface ISQLiteDb
    {
        SQLiteAsyncConnection GetConnection();
    }
}

