﻿using ProyectoNotas.Conexiones;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoNotas.Adapters
{
    public class ProyectosAdapters: ConexionMySql
    {
        private MailingHelper mailinghelper = new MailingHelper();

        public bool GuardarProyecto(ProyectosModel Proyecto)
        {

            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spGuardarProyecto (@pID, @pNombre, @pDescripcion, @pIdUsuario); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pID", Proyecto.ID);
                command.Parameters.AddWithValue("@pNombre", Proyecto.Nombre);
                command.Parameters.AddWithValue("@pDescripcion", Proyecto.Descripcion);
                command.Parameters.AddWithValue("@pIdUsuario", Proyecto.IdUsuario);
                conn.Open();
                int Result = command.ExecuteNonQuery();
                if (Result > 0)
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                mailinghelper.EnviarMensajeError("class: ProyectosAdapter, Method: GuardarProyecto, Error:" + e.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }

        public List<ProyectosModel> ExtraerProyectosUsuario(int IdUsuario)
        {
            List<ProyectosModel> ProyectosEncontrados = new List<ProyectosModel>();
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spObtenerProyectosUsuario (@pIdUsuario); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pIdUsuario", IdUsuario);
                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    ProyectosModel obProyecto = new ProyectosModel();
                    obProyecto.ID = Convert.ToInt32(reader["ID"]);
                    obProyecto.IdUsuario = Convert.ToInt32(reader["IdUsuario"]);
                    obProyecto.Nombre = reader["Nombre"].ToString();
                    obProyecto.Descripcion = reader["Descripcion"].ToString();
                    obProyecto.Habilitado = Convert.ToBoolean(reader["Habilitado"]);
                    ProyectosEncontrados.Add(obProyecto);
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: ProyectosAdapter, Método: ExtraerProyectosUsuario, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return ProyectosEncontrados;

        }
        public bool EliminarProyecto(int IdProyecto)
        {
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spEliminarProyecto (@pID); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pID", IdProyecto);
                conn.Open();
                int Result = command.ExecuteNonQuery();
                if (Result > 0)
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                mailinghelper.EnviarMensajeError("class: ProyectosAdapters, Method: EliminarProyecto, Error:" + e.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }

        public bool AgregarTareaProyecto(int IdTarea, int IdProyecto)
        {
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spAgregarTareaProyecto (@pIdTarea, @pIdProyecto); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pIdTarea", IdTarea);
                command.Parameters.AddWithValue("@pIdProyecto", IdProyecto);
                conn.Open();
                int Result = command.ExecuteNonQuery();
                if (Result > 0)
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                mailinghelper.EnviarMensajeError("class: ProyectosAdapters, Method: AgregarTareaProyecto, Error:" + e.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }
        public List<ProyectosModel> ExtraerProyectosCompartidos(int IdUsuario)
        {
            List<ProyectosModel> ProyectosEncontrados = new List<ProyectosModel>();
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spObtenerProyectosCompartidos (@pIdUsuario); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pIdUsuario", IdUsuario);
                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    ProyectosModel obProyecto = new ProyectosModel();
                    obProyecto.ID = Convert.ToInt32(reader["ID"]);
                    obProyecto.IdUsuario = Convert.ToInt32(reader["IdUsuario"]);
                    obProyecto.Nombre = reader["Nombre"].ToString();
                    obProyecto.Descripcion = reader["Descripcion"].ToString();
                    obProyecto.Habilitado = Convert.ToBoolean(reader["Habilitado"]);
                    ProyectosEncontrados.Add(obProyecto);
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: ProyectosAdapter, Método: ExtraerProyectosCompartidos, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return ProyectosEncontrados;

        }
        public List<ProyectosModel> ExtraerProyectosInvitado(int IdUsuario)
        {
            List<ProyectosModel> ProyectosEncontrados = new List<ProyectosModel>();
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spObtenerProyectosInvitado (@pIdUsuario); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pIdUsuario", IdUsuario);
                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    ProyectosModel obProyecto = new ProyectosModel();
                    obProyecto.ID = Convert.ToInt32(reader["ID"]);
                    obProyecto.IdUsuario = Convert.ToInt32(reader["IdUsuario"]);
                    obProyecto.Nombre = reader["Nombre"].ToString();
                    obProyecto.Descripcion = reader["Descripcion"].ToString();
                    obProyecto.Habilitado = Convert.ToBoolean(reader["Habilitado"]);
                    ProyectosEncontrados.Add(obProyecto);
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: ProyectosAdapter, Método: ExtraerProyectosInvitado, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return ProyectosEncontrados;

        }
    }
}
