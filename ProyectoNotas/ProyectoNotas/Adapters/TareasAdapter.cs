﻿using ProyectoNotas.Conexiones;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoNotas.Adapters
{
    public class TareasAdapter : ConexionMySql
    {
        private MailingHelper mailinghelper = new MailingHelper();

        public List<TareasModel> ExtraerTareasUsuario(int IdUsuario, int IdStatus)
        {
            List<TareasModel> TareasEncontradas = new List<TareasModel>();
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spObtenerTareasUsuario (@pIdUsuario, @pIdStatus); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pIdUsuario", IdUsuario);
                command.Parameters.AddWithValue("@pIdStatus", IdStatus);
                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    TareasModel objTarea = new TareasModel();
                    objTarea.ID = Convert.ToInt32(reader["ID"]);
                    objTarea.IdUsuario = Convert.ToInt32(reader["IdUsuario"]);
                    objTarea.Titulo = reader["Titulo"].ToString();
                    objTarea.Descripcion = reader["Descripcion"].ToString();
                    objTarea.IdEstatus = Convert.ToInt32(reader["IdEstatus"]);
                    objTarea.IdPrioridad = Convert.ToInt32(reader["IdPrioridad"]);
                    objTarea.IdProyecto = Convert.ToInt32(reader["IdProyecto"]);
                    objTarea.FechaInicio = Convert.ToDateTime(reader["FechaInicio"]);
                    objTarea.FechaFin = Convert.ToDateTime(reader["FechaFin"]);
                    switch (objTarea.IdPrioridad)
                    {
                        case 1:
                            //Baja
                            objTarea.ColorPrioridad = "Blue";
                            break;
                        case 2:
                            //Media
                            objTarea.ColorPrioridad = "Green";
                            break;
                        case 3:
                            //Alta
                            objTarea.ColorPrioridad = "Yellow";
                            break;
                        case 4:
                            //Urgente
                            objTarea.ColorPrioridad = "Red";
                            break;
                    }
                    if ((objTarea.FechaInicio < DateTime.Now & objTarea.IdEstatus == 1) | 
                        (objTarea.FechaFin < DateTime.Now & objTarea.IdEstatus == 2))
                    {
                        objTarea.MensajeVencido = "VENCIDA";
                    }
                    else
                    {
                        objTarea.MensajeVencido = "";
                    }
                    TareasEncontradas.Add(objTarea);
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: TareasAdapter, Método: ExtraerTareasUsuario, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return TareasEncontradas;

        }
        public List<TareasModel> ExtraerTareasProyecto(int IdProyecto)
        {
            List<TareasModel> TareasEncontradas = new List<TareasModel>();
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spObtenerTareasProyecto (@pIdProyecto); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pIdProyecto", IdProyecto);

                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    TareasModel objTarea = new TareasModel();
                    objTarea.ID = Convert.ToInt32(reader["ID"]);
                    objTarea.IdUsuario = Convert.ToInt32(reader["IdUsuario"]);
                    objTarea.Titulo = reader["Titulo"].ToString();
                    objTarea.Descripcion = reader["Descripcion"].ToString();
                    objTarea.IdEstatus = Convert.ToInt32(reader["IdEstatus"]);
                    objTarea.IdPrioridad = Convert.ToInt32(reader["IdPrioridad"]);
                    objTarea.IdProyecto = Convert.ToInt32(reader["IdProyecto"]);
                    objTarea.FechaInicio = Convert.ToDateTime(reader["FechaInicio"]);
                    objTarea.FechaFin = Convert.ToDateTime(reader["FechaFin"]);
                    switch (objTarea.IdPrioridad)
                    {
                        case 1:
                            //Baja
                            objTarea.ColorPrioridad = "Blue";
                            break;
                        case 2:
                            //Media
                            objTarea.ColorPrioridad = "Green";
                            break;
                        case 3:
                            //Alta
                            objTarea.ColorPrioridad = "Yellow";
                            break;
                        case 4:
                            //Urgente
                            objTarea.ColorPrioridad = "Red";
                            break;
                    }
                    if ((objTarea.FechaInicio < DateTime.Now & objTarea.IdEstatus == 1) |
                        (objTarea.FechaFin < DateTime.Now & objTarea.IdEstatus == 2))
                    {
                        objTarea.MensajeVencido = "VENCIDA";
                    }
                    else
                    {
                        objTarea.MensajeVencido = "";
                    }
                    TareasEncontradas.Add(objTarea);
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: TareasAdapter, Método: ExtraerTareasProyecto, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return TareasEncontradas;

        }
        public bool GuardarTarea(TareasModel Tarea)
        {

            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spGuardarTarea (@pID, @pIdUsuario, @pTitulo, @pDescripcion, @pIdEstatus, @pIdPrioridad, @pIdProyecto, @pFechaInicio, @pFechaFin); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pID", Tarea.ID);
                command.Parameters.AddWithValue("@pIdUsuario", Tarea.IdUsuario);
                command.Parameters.AddWithValue("@pTitulo", Tarea.Titulo);
                command.Parameters.AddWithValue("@pDescripcion", Tarea.Descripcion);
                command.Parameters.AddWithValue("@pIdEstatus", Tarea.IdEstatus);
                command.Parameters.AddWithValue("@pIdPrioridad", Tarea.IdPrioridad);
                command.Parameters.AddWithValue("@pIdProyecto", Tarea.IdProyecto);
                command.Parameters.AddWithValue("@pFechaInicio", Tarea.FechaInicio);
                command.Parameters.AddWithValue("@pFechaFin", Tarea.FechaFin);

                conn.Open();
                int Result = command.ExecuteNonQuery();
                if (Result > 0)
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                mailinghelper.EnviarMensajeError("class: TareasAdapter, Method: GuardarTarea, Error:" + e.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }
        public TareasModel TraerDetalleTarea(int IdTarea)
        {
            TareasModel objTarea = new TareasModel();

            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spTraerDetalleTarea (@pIdTarea); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pIdTarea", IdTarea);
                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    objTarea.ID = Convert.ToInt32(reader["ID"]);
                    objTarea.IdUsuario = Convert.ToInt32(reader["IdUsuario"]);
                    objTarea.Titulo = reader["Titulo"].ToString();
                    objTarea.Descripcion = reader["Descripcion"].ToString();
                    objTarea.IdEstatus = Convert.ToInt32(reader["IdEstatus"]);
                    objTarea.IdPrioridad = Convert.ToInt32(reader["IdPrioridad"]);
                    objTarea.IdProyecto = Convert.ToInt32(reader["IdProyecto"]);
                    objTarea.FechaInicio = Convert.ToDateTime(reader["FechaInicio"]);
                    objTarea.FechaFin = Convert.ToDateTime(reader["FechaFin"]);
                    switch (objTarea.IdPrioridad)
                    {
                        case 1:
                            //Baja
                            objTarea.ColorPrioridad = "Blue";
                            break;
                        case 2:
                            //Media
                            objTarea.ColorPrioridad = "Green";
                            break;
                        case 3:
                            //Alta
                            objTarea.ColorPrioridad = "Yellow";
                            break;
                        case 4:
                            //Urgente
                            objTarea.ColorPrioridad = "Red";
                            break;
                    }
                    if ((objTarea.FechaInicio < DateTime.Now & objTarea.IdEstatus == 1) |
                        (objTarea.FechaFin < DateTime.Now & objTarea.IdEstatus == 2))
                    {
                        objTarea.MensajeVencido = "VENCIDA";
                    }
                    else
                    {
                        objTarea.MensajeVencido = "";
                    }

                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: TareasAdapter, Método: ExtraerTareasUsuario, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return objTarea;

        }
        public bool ActualizarEstatusTarea(int IdTarea, int IdEstatus)
        {

            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spActualizarEstatusTarea (@pID, @pIdEstatus); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pID", IdTarea);
                command.Parameters.AddWithValue("@pIdEstatus", IdEstatus);
                conn.Open();
                int Result = command.ExecuteNonQuery();
                if (Result > 0)
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("class: TareasAdapter, Method: ActualizarEstatusTarea, Error:" + e.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }
        public bool EliminarTarea (int IdTarea)
        {
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spEliminarTarea (@pID); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pID", IdTarea);
                conn.Open();
                int Result = command.ExecuteNonQuery();
                if (Result > 0)
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                mailinghelper.EnviarMensajeError("class: TareasAdapter, Method: EliminarTarea, Error:" + e.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }
        public bool EliminarTareaProyecto(int IdTarea)
        {
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spEliminarTareaProyecto (@pID); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pID", IdTarea);
                conn.Open();
                int Result = command.ExecuteNonQuery();
                if (Result > 0)
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                mailinghelper.EnviarMensajeError("class: TareasAdapter, Method: EliminarTareaProyecto, Error:" + e.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }

    }

}

