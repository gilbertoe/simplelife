﻿using ProyectoNotas.Conexiones;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoNotas.Adapters
{
    public class NotificacionesAdapter : ConexionMySql
    {

        public List<TareasModel> TraerProximasNotas(int IdUsuario)
        {
            List<TareasModel> NotasEncontradas = new List<TareasModel>();
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spObtenerTareasUsuario (@pIdUsuario, @pIdStatus);  ");
                command = CreateCommand(query.ToString());
                command.CommandTimeout = 0;
                command.Parameters.AddWithValue("@pIdUsuario", IdUsuario);
                command.Parameters.AddWithValue("@pIdStatus", 1);
                conn.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TareasModel objTarea = new TareasModel();

                    objTarea.ID = Convert.ToInt32(reader["ID"]);
                    objTarea.IdUsuario = Convert.ToInt32(reader["IdUsuario"]);
                    objTarea.Titulo = reader["Titulo"].ToString();
                    objTarea.Descripcion = reader["Descripcion"].ToString();
                    objTarea.IdEstatus = Convert.ToInt32(reader["IdEstatus"]);
                    objTarea.IdPrioridad = Convert.ToInt32(reader["IdPrioridad"]);
                    objTarea.IdProyecto = Convert.ToInt32(reader["IdProyecto"]);
                    objTarea.FechaInicio = Convert.ToDateTime(reader["FechaInicio"]);
                    objTarea.FechaFin = Convert.ToDateTime(reader["FechaFin"]);
                    double Minutes = (objTarea.FechaInicio - DateTime.Now).TotalMinutes;
                    if (Minutes <= 10 & Minutes > 0)
                    {
                        NotasEncontradas.Add(objTarea);
                    }
                }
            }
            catch 
            {
                //mailinghelper.EnviarMensajeError("Clase: NotificacionesAdapter, Método: TraerProximasNotas, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return NotasEncontradas;
        }
    }
}
