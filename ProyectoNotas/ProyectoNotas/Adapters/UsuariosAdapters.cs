﻿using MySqlConnector;
using ProyectoNotas.Conexiones;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ProyectoNotas.Adapters
{
    public class UsuariosAdapters : ConexionMySql
    {
        private string ContrasenaEncrypt = "G1l3sci9BB";
        private MailingHelper mailinghelper = new MailingHelper();
        public UsuariosModel IniciarSesion(string Email, string Contrasena)
        {
            UsuariosModel UsuarioSesionIniciada = new UsuariosModel();
            ShaHelper objSha = new ShaHelper();
            try
            {
                string ContrasenaEncriptada = objSha.Encrypt(Contrasena, ContrasenaEncrypt);
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spIniciarSesion (@pEmail, @pContrasena)");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pEmail", Email);
                command.Parameters.AddWithValue("@pContrasena", ContrasenaEncriptada);
                conn.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UsuarioSesionIniciada.ID = Convert.ToInt32(reader["ID"]);
                    UsuarioSesionIniciada.Nombres = reader["Nombres"].ToString();
                    UsuarioSesionIniciada.Email = reader["Email"].ToString();
                    UsuarioSesionIniciada.Contrasena = objSha.Decrypt(reader["Contrasena"].ToString(), ContrasenaEncrypt);
                    UsuarioSesionIniciada.Telefono = reader["Telefono"].ToString();
                    UsuarioSesionIniciada.FechaRegistro = Convert.ToDateTime(reader["FechaRegistro"]);
                    UsuarioSesionIniciada.Estatus = Convert.ToInt32(reader["Estatus"]);
                    if (!string.IsNullOrEmpty(reader["FechaBaja"].ToString()))
                    {
                        UsuarioSesionIniciada.FechaBaja = Convert.ToDateTime(reader["FechaBaja"]);
                    }
                    UsuarioSesionIniciada.Validado = Convert.ToBoolean(reader["Validado"]);
                    UsuarioSesionIniciada.ApellidoPaterno = reader["ApellidoPaterno"].ToString();
                    UsuarioSesionIniciada.ApellidoMaterno = reader["ApellidoMaterno"].ToString();
                    if (!string.IsNullOrEmpty(reader["Foto"].ToString()))
                    {
                        UsuarioSesionIniciada.Foto = (byte[])reader["Foto"];
                    }
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: UsuariosAdapter, Método: IniciarSesion, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return UsuarioSesionIniciada;
        }
        public int RegistrarUsuario(UsuariosModel NuevoUsuario)
        {
            int modified = 0;
            ShaHelper objSha = new ShaHelper();
            try
            {
                string ContrasenaEncriptada = objSha.Encrypt(NuevoUsuario.Contrasena, ContrasenaEncrypt);
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spRegistroUsuario(@pNombres, @pApellidoPaterno, @pApellidoMaterno, @pCodigoConfirmacion, @pEmail, @pContrasena, @pTelefono, @pFoto, @pFechaRegistro); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pNombres", NuevoUsuario.Nombres);
                command.Parameters.AddWithValue("@pApellidoPaterno", NuevoUsuario.ApellidoPaterno);
                command.Parameters.AddWithValue("@pApellidoMaterno", NuevoUsuario.ApellidoMaterno);
                command.Parameters.AddWithValue("@pCodigoConfirmacion", NuevoUsuario.CodigoConfirmacion);
                command.Parameters.AddWithValue("@pEmail", NuevoUsuario.Email);
                command.Parameters.AddWithValue("@pContrasena", ContrasenaEncriptada);
                command.Parameters.AddWithValue("@pTelefono", NuevoUsuario.Telefono);
                if (NuevoUsuario.Foto != null)
                {
                    var paramUserImage = new MySqlParameter("@pFoto", MySqlDbType.Blob, NuevoUsuario.Foto.Length);
                    paramUserImage.Value = NuevoUsuario.Foto;
                    command.Parameters.Add(paramUserImage);
                }
                else
                {
                    command.Parameters.AddWithValue("@pFoto", null);
                }

                command.Parameters.AddWithValue("@pFechaRegistro", DateTime.Now);
                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    modified = Convert.ToInt32(reader["LID"]);
                }
                return modified;
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: UsuariosAdapter, Método: RegistrarUsuario, Error: " + ex.Message);

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
        public bool ActualizarUsuario(UsuariosModel UsuarioActual)
        {
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spActualizarUsuario(@pIdUsuario, @pNombres, @pTelefono, @pApellidoPaterno, @pApellidoMaterno, @pFoto); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pIdUsuario", UsuarioActual.ID);
                command.Parameters.AddWithValue("@pNombres", UsuarioActual.Nombres);
                command.Parameters.AddWithValue("@pTelefono", UsuarioActual.Telefono);
                command.Parameters.AddWithValue("@pApellidoPaterno", UsuarioActual.ApellidoPaterno);
                command.Parameters.AddWithValue("@pApellidoMaterno", UsuarioActual.ApellidoMaterno);
                if (UsuarioActual.Foto != null)
                {
                    var paramUserImage = new MySqlParameter("@pFoto", MySqlDbType.Blob, UsuarioActual.Foto.Length);
                    paramUserImage.Value = UsuarioActual.Foto;
                    command.Parameters.Add(paramUserImage);
                }
                else
                {
                    command.Parameters.AddWithValue("@pFoto", null);
                }
                conn.Open();
                int Result = command.ExecuteNonQuery();
                if (Result > 0)
                {
                    return true;
                }

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: UsuariosAdapter, Método: ActualizarUsuario, Error: " + ex.Message);

            }
            finally
            {
                conn.Close();
            }
            return false;
        }
        public bool ExisteUsuario(string Email)
        {
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spGetUsuarioExistente (@pEmail)");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pEmail", Email);
                conn.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: UsuariosAdapter, Método: ExisteUsuario, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }
        public string ExtraerContrasena(string Email)
        {
            string contrasena = "";
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spExtraerContrasena(@pEmail) ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pEmail", Email);
                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    contrasena = reader["contrasena"].ToString();
                    return contrasena;
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: UsuariosAdapter, Método: ExtraerContrasena, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return contrasena;
        }
        public bool ValidarCuenta(string Email, string Codigo)
        {
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spValidarCuenta (@pEmail, @pCodigo)");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pEmail", Email);
                command.Parameters.AddWithValue("@pCodigo", Codigo);
                conn.Open();
                int Result = command.ExecuteNonQuery();
                if (Result > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: UsuariosAdapter, Método: ValidarCuenta, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }

        public bool ActualizarPassword(int IdUsuario, string NuevoPass)
        {
            ShaHelper objSha = new ShaHelper();
            try
            {
                string ContrasenaEncriptada = objSha.Encrypt(NuevoPass, ContrasenaEncrypt);
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spActualizarContrasena(@pIdUsuario, @pNuevaContrasena); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pIdUsuario", IdUsuario);
                command.Parameters.AddWithValue("@pNuevaContrasena", ContrasenaEncriptada);
                conn.Open();
                int Result = command.ExecuteNonQuery();
                if (Result > 0)
                {
                    return true;
                }

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: UsuariosAdapter, Método: ActualizarPassword, Error: " + ex.Message);

            }
            finally
            {
                conn.Close();
            }
            return false;

        }
        public bool ValidarContrasena(string Email, string Password)
        {
            ShaHelper objSha = new ShaHelper();
            string BdEmail = "";
            try
            {
                string ContrasenaEncriptada = objSha.Encrypt(Password, ContrasenaEncrypt);
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spValidarContrasena(@pContrasena); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pContrasena", ContrasenaEncriptada);
                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    BdEmail = reader["Email"].ToString();
                }
                if(string.IsNullOrEmpty(BdEmail))
                {
                    //LA CONTRASEÑA NO EXISTE EN SISTEMA
                    return false;
                }
                if(BdEmail.Equals(Email))
                {
                    return true;
                }
                else
                {
                    //LA CONTRASEÑA NO HACE MATCH CON EL MAIL
                    return false;
                }

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: UsuariosAdapter, Método: ValidarContrasena, Error: " + ex.Message);

            }
            finally
            {
                conn.Close();
            }
            return false;
        }

        public bool ValidarCorreo(string Email)
        {
            string BdEmail = "";
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spValidarEmail (@pEmail)");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pEmail", Email);
                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {

                    BdEmail = reader["Email"].ToString();
                    
                }
                if (string.IsNullOrEmpty(BdEmail))
                {
                    //El correo no existe 
                    return false;
                }
                return true;

            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: UsuariosAdapter, Método: ValidarCorreo, Error: " + ex.Message);

            }
            finally
            {
                conn.Close();
            }
            return false;
        }

    }
}
