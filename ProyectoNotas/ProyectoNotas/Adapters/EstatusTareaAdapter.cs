﻿using ProyectoNotas.Conexiones;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoNotas.Adapters
{
    public class EstatusTareaAdapter : ConexionMySql
    {
        private MailingHelper mailinghelper = new MailingHelper();

        public List<EstatusTareaModel> ExtraerEstatusTarea()
        {
            List<EstatusTareaModel> EstatusTareaEncontrados = new List<EstatusTareaModel>();
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spTraerEstatusTarea(); ");
                command = CreateCommand(query.ToString());

                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    EstatusTareaModel objEstatusTarea = new EstatusTareaModel();
                    objEstatusTarea.ID = Convert.ToInt32(reader["ID"]);
                    objEstatusTarea.Nombre = reader["Nombre"].ToString();
                    EstatusTareaEncontrados.Add(objEstatusTarea);
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: EstatusTareaAdapter, Método: ExtraerEstatusTarea, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return EstatusTareaEncontrados;

        }

    }
}
