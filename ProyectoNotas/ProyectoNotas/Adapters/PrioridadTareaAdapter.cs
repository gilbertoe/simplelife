﻿using ProyectoNotas.Conexiones;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoNotas.Adapters
{
    public class PrioridadTareaAdapter: ConexionMySql
    {
        private MailingHelper mailinghelper = new MailingHelper();

        public List<PrioridadTareaModel> ExtraerPrioridadTarea()
        {
            List<PrioridadTareaModel> PrioridadTareaEncontrados = new List<PrioridadTareaModel>();
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spTraerPrioridadTarea(); ");
                command = CreateCommand(query.ToString());

                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    PrioridadTareaModel objPrioridadTarea = new PrioridadTareaModel();
                    objPrioridadTarea.ID = Convert.ToInt32(reader["ID"]);
                    objPrioridadTarea.Nombre = reader["Nombre"].ToString();
                    PrioridadTareaEncontrados.Add(objPrioridadTarea);
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: PrioridadTareaAdapter, Método: ExtraerPrioridadTarea, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return PrioridadTareaEncontrados;

        }
    }
}
