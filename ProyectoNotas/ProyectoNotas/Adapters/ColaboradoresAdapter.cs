﻿using ProyectoNotas.Conexiones;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoNotas.Adapters
{
    public class ColaboradoresAdapter : ConexionMySql
    {
        //private string ContrasenaEncrypt = "G1l3sci9BB";
        private MailingHelper mailinghelper = new MailingHelper();
        public List<ColaboradoresModel> ObtenerColaboradoresUsuario(int IdUsuario, int EsInvitado)
        {
            List<ColaboradoresModel> ColaboradoresEncontrados = new List<ColaboradoresModel>();
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spObtenerColaboradoresUsuario (@pIdUsuario, @pInvitado); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pIdUsuario", IdUsuario);
                command.Parameters.AddWithValue("@pInvitado", EsInvitado);

                conn.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ColaboradoresModel objColaborador = new ColaboradoresModel();
                    objColaborador.ID = Convert.ToInt32(reader["ID"]);
                    objColaborador.IdUsuario = Convert.ToInt32(reader["IdUsuario"]);
                    objColaborador.IdColaborador = Convert.ToInt32(reader["IdColaborador"]);
                    objColaborador.IdProyecto = Convert.ToInt32(reader["IdProyecto"]);
                    objColaborador.InvitacionAceptada = Convert.ToBoolean(reader["InvitacionAceptada"]);
                    objColaborador.ColaboracionVigente = Convert.ToBoolean(reader["ColaboracionVigente"]);
                    objColaborador.IdStatusColaboracion = Convert.ToInt32(reader["IdStatusColaboracion"]);
                    objColaborador.Usuario.Nombres = reader["Nombres"].ToString();
                    objColaborador.Usuario.ApellidoPaterno = reader["ApellidoPaterno"].ToString();
                    objColaborador.Usuario.Email = reader["Email"].ToString();
                    ColaboradoresEncontrados.Add(objColaborador);
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: ColaboradoresAdapter, Método: ObtenerColaboradoresUsuario, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return ColaboradoresEncontrados;
        }
        public bool RemoverColaboracion(int IdColaboracion)
        {
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spRemoverColaboracion (@pIdColaboracion); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@IdColaboracion", IdColaboracion);
                conn.Open();
                int Result = command.ExecuteNonQuery();
                if (Result > 0)
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                mailinghelper.EnviarMensajeError("class: ColaboradoresAdapter, Method: RemoverColaboracion, Error:" + e.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }
        public UsuariosModel ExtraerColaborador(string Email)
        {
            UsuariosModel UsuarioSesionIniciada = new UsuariosModel();
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spValidarCorreo (@pEmail)");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pEmail", Email);
                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    UsuarioSesionIniciada.ID = Convert.ToInt32(reader["ID"]);
                    UsuarioSesionIniciada.Nombres = reader["Nombres"].ToString();
                    UsuarioSesionIniciada.Email = reader["Email"].ToString();
                    UsuarioSesionIniciada.Telefono = reader["Telefono"].ToString();
                    UsuarioSesionIniciada.Estatus = Convert.ToInt32(reader["Estatus"]);
                    if (!string.IsNullOrEmpty(reader["FechaBaja"].ToString()))
                    {
                        UsuarioSesionIniciada.FechaBaja = Convert.ToDateTime(reader["FechaBaja"]);
                    }
                    UsuarioSesionIniciada.Validado = Convert.ToBoolean(reader["Validado"]);
                    UsuarioSesionIniciada.ApellidoPaterno = reader["ApellidoPaterno"].ToString();
                    UsuarioSesionIniciada.ApellidoMaterno = reader["ApellidoMaterno"].ToString();
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: ColaboradoresAdapter, Método: ExtraerColaborador, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return UsuarioSesionIniciada;
        }
        public bool GuardarColaboracion(ColaboradoresModel Colaboracion)
        {

            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spGuardarColaboracion (@pIdUsuario, @pIdColaborador, @pIdProyecto, @pCodigoColaboracion); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pIdUsuario", Colaboracion.IdUsuario);
                command.Parameters.AddWithValue("@pIdColaborador", Colaboracion.IdColaborador);
                command.Parameters.AddWithValue("@pIdProyecto", Colaboracion.IdProyecto);
                command.Parameters.AddWithValue("@pCodigoColaboracion", Colaboracion.CodigoColaboracion);

                conn.Open();
                int Result = command.ExecuteNonQuery();
                if (Result > 0)
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                mailinghelper.EnviarMensajeError("class: ColaboradoresAdapter, Method: GuardarColaboracion, Error:" + e.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }
        public List<ColaboradoresModel> ObtenerSolicitudColaborarEnviada(int IdUsuario)
        {
            List<ColaboradoresModel> ColaboradoresEncontrados = new List<ColaboradoresModel>();
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spObtenerSolicitudColaborarEnviada (@pIdUsuario); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pIdUsuario", IdUsuario);
                conn.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ColaboradoresModel objColaborador = new ColaboradoresModel();
                    objColaborador.ID = Convert.ToInt32(reader["ID"]);
                    objColaborador.IdUsuario = Convert.ToInt32(reader["IdUsuario"]);
                    objColaborador.IdColaborador = Convert.ToInt32(reader["IdColaborador"]);
                    objColaborador.IdProyecto = Convert.ToInt32(reader["IdProyecto"]);
                    objColaborador.InvitacionAceptada = Convert.ToBoolean(reader["InvitacionAceptada"]);
                    objColaborador.ColaboracionVigente = Convert.ToBoolean(reader["ColaboracionVigente"]);
                    objColaborador.NombreProyecto = reader["NombreProyecto"].ToString();
                    objColaborador.StatusColaboracion = reader["StatusColaboracion"].ToString();
                    objColaborador.IdStatusColaboracion = Convert.ToInt32(reader["IdStatusColaboracion"]);
                    objColaborador.Usuario.Nombres = reader["Nombres"].ToString();
                    objColaborador.Usuario.ApellidoPaterno = reader["ApellidoPaterno"].ToString();
                    objColaborador.Usuario.Email = reader["Email"].ToString();

                    ColaboradoresEncontrados.Add(objColaborador);
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: ColaboradoresAdapter, Método: ObtenerSolicitudColaborarEnviada, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return ColaboradoresEncontrados;
        }
        public List<ColaboradoresModel> ObtenerSolicitudColaborarRecibida(int IdUsuario)
        {
            List<ColaboradoresModel> ColaboradoresEncontrados = new List<ColaboradoresModel>();
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spObtenerSolicitudColaborarRecibida (@pIdUsuario); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pIdUsuario", IdUsuario);
                conn.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ColaboradoresModel objColaborador = new ColaboradoresModel();
                    objColaborador.ID = Convert.ToInt32(reader["ID"]);
                    objColaborador.IdUsuario = Convert.ToInt32(reader["IdUsuario"]);
                    objColaborador.IdColaborador = Convert.ToInt32(reader["IdColaborador"]);
                    objColaborador.IdProyecto = Convert.ToInt32(reader["IdProyecto"]);
                    objColaborador.InvitacionAceptada = Convert.ToBoolean(reader["InvitacionAceptada"]);
                    objColaborador.ColaboracionVigente = Convert.ToBoolean(reader["ColaboracionVigente"]);
                    objColaborador.StatusColaboracion = reader["StatusColaboracion"].ToString();
                    objColaborador.IdStatusColaboracion = Convert.ToInt32(reader["IdStatusColaboracion"]);
                    objColaborador.NombreProyecto = reader["NombreProyecto"].ToString();
                    objColaborador.Usuario.Nombres = reader["Nombres"].ToString();
                    objColaborador.Usuario.ApellidoPaterno = reader["ApellidoPaterno"].ToString();
                    objColaborador.Usuario.Email = reader["Email"].ToString();

                    ColaboradoresEncontrados.Add(objColaborador);
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: ColaboradoresAdapter, Método: ObtenerSolicitudColaborarRecibida, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return ColaboradoresEncontrados;
        }
        public bool ActualizarColaboracion(ColaboradoresModel Colaboracion)
        {
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" CALL spActualizarColaboracion (@pID, @pIdStatusColaboracion, @pColaboracionVigente, @pInvitacionAceptada); ");
                command = CreateCommand(query.ToString());
                command.Parameters.AddWithValue("@pID", Colaboracion.ID);
                command.Parameters.AddWithValue("@pIdStatusColaboracion", Colaboracion.IdStatusColaboracion);
                command.Parameters.AddWithValue("@pColaboracionVigente", Colaboracion.ColaboracionVigente);
                command.Parameters.AddWithValue("@pInvitacionAceptada", Colaboracion.InvitacionAceptada);
                conn.Open();
                int Result = command.ExecuteNonQuery();
                if (Result > 0)
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                mailinghelper.EnviarMensajeError("class: ColaboradoresAdapter, Method: ActualizarColaboracion, Error:" + e.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }
    }
}
