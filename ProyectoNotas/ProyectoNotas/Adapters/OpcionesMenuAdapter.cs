﻿using ProyectoNotas.Conexiones;
using ProyectoNotas.Helpers;
using ProyectoNotas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ProyectoNotas.Adapters
{
    public class OpcionesMenuAdapter : ConexionMySql
    {

        private MailingHelper mailinghelper = new MailingHelper();

        public List<OpcionesMenuModel> ObtenerOpcionesMenu()
        {
            List<OpcionesMenuModel> objOpcionesMenu = new List<OpcionesMenuModel>();
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append(" SELECT * FROM vwGetAllMenusActives ");
                command = CreateCommand(query.ToString());
                conn.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    OpcionesMenuModel OpcionMenu = new OpcionesMenuModel();
                    OpcionMenu.IdOpcionesMenu = Convert.ToInt32(reader["intIdopcionesMenu"]);
                    OpcionMenu.Nombre = reader["vchNombre"].ToString();
                    OpcionMenu.Foto = reader["binImagen"].ToString();
                    OpcionMenu.Orden = Convert.ToInt32(reader["intOrden"].ToString());
                    OpcionMenu.Visible = Convert.ToBoolean(reader["bitVisible"]);
                    objOpcionesMenu.Add(OpcionMenu);
                }
            }
            catch (Exception ex)
            {
                mailinghelper.EnviarMensajeError("Clase: OpcionesMenuAdapter, Método: ObtenerOpcionesMenu, Error: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return objOpcionesMenu;
        }
    }
}
