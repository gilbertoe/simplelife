﻿using Android.Content;
using Android.Provider;
using System;
using System.Threading.Tasks;
using ProyectoNotas.Droid.Services;
using ProyectoNotas.Services;
using Java.Util;

[assembly: Xamarin.Forms.Dependency(typeof(CalendarService))]
namespace ProyectoNotas.Droid.Services
{
    public class CalendarService : ICalendarService
    {
        public Task<(bool isAdded, string message)> AddEventToCalendar(DateTime startDate, DateTime endDate, string title, string description, string location)
        {
            bool isEventAdded = true;
            string message = string.Empty;
            TimeSpan ts = new DateTimeOffset(DateTime.Now).Offset;
            try
            {
                Intent eventValues = new Intent(Intent.ActionInsert);
                eventValues.SetData(CalendarContract.Events.ContentUri);
                eventValues.AddFlags(ActivityFlags.NewTask);
                eventValues.PutExtra(CalendarContract.Events.InterfaceConsts.CalendarId, 2);
                eventValues.PutExtra(CalendarContract.Events.InterfaceConsts.Title, title);
                eventValues.PutExtra(CalendarContract.Events.InterfaceConsts.Description, description);
                eventValues.PutExtra(CalendarContract.ExtraEventBeginTime, GetDateTimeMS(startDate.Year, startDate.Month, startDate.Day, startDate.Hour, startDate.Minute));
                eventValues.PutExtra(CalendarContract.ExtraEventEndTime, GetDateTimeMS(endDate.Year, endDate.Month, endDate.Day, endDate.Hour, endDate.Minute));
                eventValues.PutExtra(CalendarContract.Events.InterfaceConsts.EventTimezone, TimeZoneInfo.Local.DisplayName);
                eventValues.PutExtra(CalendarContract.Events.InterfaceConsts.EventEndTimezone, TimeZoneInfo.Local.DisplayName);
                eventValues.PutExtra(CalendarContract.Events.InterfaceConsts.EventLocation, location);
                Android.App.Application.Context.StartActivity(eventValues);
            }
            catch 
            {
                isEventAdded = false;
            }
            return Task.FromResult((isEventAdded, message));
        }
        private long GetDateTimeMS(int yr, int month, int day, int hr, int min)
        {
            Calendar c = Calendar.GetInstance(Java.Util.TimeZone.Default);

            c.Set(CalendarField.DayOfMonth, day);
            c.Set(CalendarField.HourOfDay, hr);
            c.Set(CalendarField.Minute, min);
            c.Set(CalendarField.Month, month - 1);
            c.Set(CalendarField.Year, yr);

            return c.TimeInMillis;
        }
    }
}